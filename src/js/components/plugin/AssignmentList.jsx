import Dashboard, { React } from "Dashboard";
import { Table, Button } from "semantic-ui-react";
const { Component } = React;

export default class AssignmentList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortOrder: "descending",
      sortColumn: null
    };

    String.prototype.capitalizeFirstLetter = function() {
      return (this.charAt(0).toUpperCase() + this.slice(1)).replace("_", " ");
    };
  }

  render() {
    let { assignments, visibleColumns, reset, removeColumn } = this.props;

    assignments = this.sort(assignments);

    let headersCell = [];
    visibleColumns.forEach(columnKey => {
      let column = visibleColumns.length > 1 ? <a href="#" onClick={() => removeColumn(columnKey)} style={{ color: "black", float: "right" }} className={"icon dripicons-cross"} /> : "";
      headersCell.push(
        <Table.HeaderCell key={columnKey.propertyKey}>
          <HeaderSorter columnKey={columnKey} state={this.state} handleClick={() => this.handleSortClick(columnKey)} />
          {column}
        </Table.HeaderCell>
      );
    });
    headersCell.push(<Table.HeaderCell key="action" width={1} />);

    let tableRows = [];
    for (var node in assignments) {
      tableRows.push(<AssignmentRow key={assignments[node].id} assignment={assignments[node]} reset={reset} visibleColumns={visibleColumns} />);
    }

    return (
      <Table striped compact celled>
        <Table.Header>
          <Table.Row>
            {headersCell}
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {tableRows}
        </Table.Body>
      </Table>
    );
  }

  handleSortClick(columnProperty) {
    if (columnProperty === this.state.sortColumn) {
      this.setState({
        sortOrder: this.state.sortOrder === "ascending" ? "descending" : "ascending"
      });
    } else {
      this.setState({
        sortOrder: "descending",
        sortColumn: columnProperty
      });
    }
  }

  sort(sortingObjects) {
    let assignments = Array.from(sortingObjects);

    let sort = (a, b) => {
      switch (this.state.sortColumn.propertyClass) {
        case "java.util.Date":
        case "java.lang.Integer":
        case "java.lang.Long":
        case "java.lang.Double":
          if (this.state.sortOrder === "ascending") {
            return a.id - b.id;
          } else {
            return b.id - a.id;
          }
        case "java.lang.String":
          if (this.state.sortOrder === "ascending") {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
          } else {
            if (b.name < a.name) return -1;
            if (b.name > a.name) return 1;
            return 0;
          }
        case "se.infomaker.newspilot.service.common.ServiceProviderType":
          if (this.state.sortOrder === "asending") {
            if (a.type < b.type) return -1;
            if (a.type > b.type) return 1;
            return 0;
          } else {
            if (b.type < a.type) return -1;
            if (b.type > a.type) return 1;
            return 0;
          }

      }
    };
    if (this.state.sortColumn !== null) {
      assignments.sort((a, b) => sort(a, b));
    }

    return assignments;
  }
}

class HeaderSorter extends Component {
  render() {
    let { columnKey, state } = this.props;
    let icon = "icon dripicons-chevron-" + (state.sortOrder === "ascending" ? "up" : "down");
    if (state.sortColumn !== columnKey) {
      return <span><a href="#" onClick={() => this.props.handleClick()} style={{ color: "black" }}>{columnKey.valueKey.capitalizeFirstLetter()}</a></span>;
    } else {
      return (
        <span>
          <a href="#" onClick={() => this.props.handleClick()} style={{ color: "black" }}>{columnKey.valueKey.capitalizeFirstLetter()}</a>
          <span
            className={icon}
            style={{
              fontSize: 12,
              paddingTop: 17,
              textAlign: "bottom"
            }}
          />
        </span>
      );
    }
  }
}

class AssignmentRow extends Component {
  render() {
    let { assignment, reset, visibleColumns } = this.props;
    let cells = [];

    visibleColumns.forEach(columnKey => {
      let key = columnKey.propertyKey + "_" + assignment.id;
      cells.push(<NpTextCell key={key} columnInfo={columnKey} value={assignment[columnKey.valueKey]} />);
    });
    cells.push(<Table.Cell textAlign="center" collapsing key={"Reset_" + assignment.id}><Button size="mini" color="grey" compact disabled={assignment.status === 100} onClick={() => reset(assignment.id)}>Rerun</Button></Table.Cell>);
    return (
      <Table.Row negative={assignment.status === 103} positive={assignment.status === 102} warning={assignment.status === 101}>
        {cells}
      </Table.Row>
    );
  }
}

class NpTextCell extends Component {
  render() {
    let { columnInfo, value } = this.props;

    let align = "left";
    switch (columnInfo.propertyClass) {
      case "java.util.Date":
      case "java.lang.Integer":
      case "java.lang.Long":
      case "java.lang.Double":
        align = "right";

    }

    if (columnInfo.propertyClass === "java.util.Date") {
      value = Dashboard.moment(value).format("LTS");
    } else if (columnInfo.propertyClass === "java.lang.Boolean") {
      value = value ? "true" : "false";
    }

    return <Table.Cell textAlign={align} collapsing>{value}</Table.Cell>;
  }
}
