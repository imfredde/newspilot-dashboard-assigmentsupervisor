import Dashboard, { React } from "Dashboard";

const { Component } = React;

export default class ServiceCombo extends Component {
  constructor(props) {
    super(props);
  }

  contentClicked(event, key) {
    const target = event.currentTarget;
    const child = target.parentElement.parentElement.getElementsByClassName("dropbtn")[0];
    child.innerHTML = target.innerHTML;
    this.props.callbackFunction(key);
  }

  render() {
    const keyValueMap = this.props.keyValueMap;
    const initialValue = this.props.initialValue;
    let self = this;
    let contentList = [];

    keyValueMap.forEach((value, key) => {
      contentList.push(<a href="#" key={Dashboard.createUUID()} onClick={event => self.contentClicked(event, key)}>{value}</a>);
    });

    return (
      <div className="drop">
        <span className="btnWrapper">
          <span className="dropbtn">{keyValueMap.get(initialValue)}</span>
          <span className="icon dripicons-chevron-down" style={{ paddingLeft: "10px" }} />
        </span>
        <div className="dropdown-content">{contentList}</div>
      </div>
    );
  }
}
