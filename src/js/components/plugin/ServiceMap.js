export const serviceFilterKeyValue = new Map();

serviceFilterKeyValue.set("all", "All");
serviceFilterKeyValue.set("mediadistiller", "Mediadistiller");
serviceFilterKeyValue.set("fileflow", "Fileflow");
serviceFilterKeyValue.set("importer", "Importer");
serviceFilterKeyValue.set("integrationgateway", "Integration gateway");
serviceFilterKeyValue.set("printservice", "Printservice");
serviceFilterKeyValue.set("pagesync", "PageSync");
serviceFilterKeyValue.set("opencontent", "OpenContent");
serviceFilterKeyValue.set("filebackup", "Filebackup");
serviceFilterKeyValue.set("serviceplatform", "Service Platform");
serviceFilterKeyValue.set("pagerobot", "Page Robot");
serviceFilterKeyValue.set("mediaexport", "Mediaexport");

export const statusFilterKeyValue = new Map();

statusFilterKeyValue.set(-1, "All");
statusFilterKeyValue.set(102, "Success");
statusFilterKeyValue.set(100, "Waiting");
statusFilterKeyValue.set(101, "Processing");
statusFilterKeyValue.set(103, "Error");
//
//
// module.exports = {
//   serviceFilterKeyValue,
//   serviceSortKeyValue
// };
