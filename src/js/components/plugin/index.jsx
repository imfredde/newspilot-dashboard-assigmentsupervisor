import ServiceCombo from "./ServiceCombo.jsx";
import AssignmentList from "./AssignmentList.jsx";
import DatePicker from "react-datepicker";

const { serviceFilterKeyValue, statusFilterKeyValue } = require("./ServiceMap.js");
const npMappings = require("./keybasedEntityMapping").default;

const FilterType = {
  SERVICE: 1,
  STATUS: 2
};

((Dashboard, React) => {
  class Application extends Dashboard.Application {
    constructor(props) {
      super(props);

      this.nodeMap = new Map();

      this.serviceFilter = "all", this.statusFilter = -1;

      this.lastQueryId = 0;

      this.state = {
        queryId: 0,
        eventType: 0,
        nodes: [],
        visibleColumns: [],
        moment: Dashboard.moment()
      };

      this.on("newspilot:queryUpdated", userData => {
        this.updateNodes(userData);
      });

      this.onUnload = this.onUnload.bind(this);

      String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
      };

      Dashboard.moment.locale("sv");
    }

    notifyMe(values) {
      if (values.status === 103) {
        console.log("Would like to send a Notification");
        //        if (values.status === 103) return;
        if (!("Notification" in window)) {
          return;
        } else if (Notification.permission === "granted") {
          this.sendNotification(values);
        } else if (Notification.permission !== "denied") {
          Notification.requestPermission(permission => {
            if (permission === "granted") {
              this.sendNotification(values);
            }
          });
        }
      }
    }

    sendNotification(values) {
      let icon = require("./icons/" + values.type + ".png");

      var options = {
        body: values.errormessage,
        icon: icon
      };
      new Notification("ERROR " + values.type.capitalizeFirstLetter(), options);
    }

    updateNodes(userData) {
      const queryId = userData.queryId;
      const entityType = userData.entityType;
      const events = userData.events;
      console.log("AssignmentSupervisor.updateNodes: queryId=" + queryId + ", entityType=" + entityType + ", nodes=" + events.length);

      for (let event of events) {
        switch (event.eventType) {
          case "CREATE":
          case "CHANGE":
            this.nodeMap.set(event.id, event.currentValues);
            if (events.length === 1) {
              this.notifyMe(event.currentValues);
            }
            break;
          case "REMOVE":
            this.nodeMap.delete(event.id);
            break;
          default:
            console.log("Unknown eventType", event.eventType);
        }
      }

      const nodes = [...this.nodeMap.values()];
      return this.setState({ queryId, entityType, nodes });
    }

    render() {
      const GUI = Dashboard.GUI;

      let { nodes, entityType, visibleColumns } = this.state;

      let entityInfo = npMappings[entityType];
      const invisibleColumns = [];
      for (let property in entityInfo) {
        if (entityInfo.hasOwnProperty(property)) {
          let find = visibleColumns.find(element => {
            return element.propertyKey === property;
          });
          if (!find) {
            invisibleColumns.push({
              content: property,
              onClick: () => this.addColumn(property)
            });
          }
        }
      }

      return (
        <GUI.Wrapper className="@plugin_bundle_class">
          <GUI.Title text="Newspilot Assignment Supervisor" />
          <div style={{ paddingLeft: 6, paddingBottom: 10 }}>
            <label>Filter by type:</label><ServiceCombo keyValueMap={serviceFilterKeyValue} initialValue={this.serviceFilter} callbackFunction={selectedFilterValue => this.updateFilter(FilterType.SERVICE, selectedFilterValue)} />
            <label>Filter by status:</label><ServiceCombo keyValueMap={statusFilterKeyValue} initialValue={this.statusFilter} callbackFunction={selectedFilterValue => this.updateFilter(FilterType.STATUS, selectedFilterValue)} />
            <label>Date: </label><DatePicker todayButton={"Today"} dateFormat="YYYYMMDD" selected={this.state.moment} onChange={event => this.dateChanged(event)} todayButton={"Today"} />
            <GUI.DropMenu items={invisibleColumns} style={{ float: "right" }} />
          </div>

          <AssignmentList
            assignments={nodes}
            reset={id => this.resetStatus(id)}
            visibleColumns={visibleColumns}
            removeColumn={columnKey => {
              this.removeColumn(columnKey);
            }}
          />
        </GUI.Wrapper>
      );
    }

    addColumn(columnKey) {
      this.setState(({ visibleColumns }) => ({
        visibleColumns: [...visibleColumns, npMappings[this.state.entityType][columnKey]]
      }));
    }

    removeColumn(columnKey) {
      let newColumnOrder = this.state.visibleColumns.filter(column => column !== columnKey);
      this.setState({
        visibleColumns: newColumnOrder
      });
    }

    dateChanged(moment) {
      this.setState({ moment });
      this.updateQuery(moment);
    }

    updateFilter(filterType, key) {
      switch (filterType) {
        case FilterType.SERVICE:
          this.serviceFilter = key;
          break;
        case FilterType.STATUS:
          this.statusFilter = key;
          break;
      }
      this.updateQuery();
    }

    componentWillMount() {
      let moment = this.state.moment;
      this.cache("newspilot-assignment-supervisor-data", data => {
        if (data != null) {
          this.serviceFilter = data.serviceFilter || this.serviceFilter, this.statusFilter = data.statusFilter || this.statusFilter;
          if (data.moment) {
            moment = Dashboard.moment(data.moment);
            this.setState({ moment });
          }
          if (data.visibleColumns) {
            this.setState({ visibleColumns: data.visibleColumns });
          }
        }
      });
    }

    componentDidMount() {
      let moment = this.state.moment;
      window.addEventListener("beforeunload", this.onUnload);
      this.ready("se.infomaker.dashboard-agent-plugin", () => {
        this.addQuery(moment);
      });
    }

    componentWillUnmount() {
      window.removeEventListener("beforeunload", this.onUnload);
    }

    onUnload() {
      this.cache("newspilot-assignment-supervisor-data", {
        serviceFilter: this.serviceFilter,
        statusFilter: this.statusFilter,
        moment: this.state.moment,
        visibleColumns: this.state.visibleColumns
      });
      this.send("np:disconnect", {});
    }

    updateQuery(moment) {
      this.ready("se.infomaker.dashboard-agent-plugin", () => {
        if (this.lastQueryId !== 0) {
          this.nodeMap = new Map();
          console.log("AssignmentSupervisor.updateQuery: Remove Query=" + this.state.queryId);
          this.send("newspilot:removeQuery", {
            quid: this.lastQueryId
          });
        }

        this.addQuery(moment);
      });
    }

    addQuery(moment) {
      this.lastQueryId = Dashboard.createUUID();
      console.log("AssignmentSupervisor.addQuery: id=" + this.lastQueryId);
      this.send("newspilot:addQuery", {
        query: this.createQuery(moment),
        quid: this.lastQueryId
      });
    }

    createQuery(moment = this.state.moment) {
      let query = [];
      query.push('<query type="Assignment" version="1.1"><base-query><and>');

      query.push('<between field="created" type="Assignment" value1="' + moment.format("YYYY-MM-DD 00:00:00") + '" value2="..."/>');

      if (this.serviceFilter !== "all") {
        query.push('<eq field="type" type="Assignment" value="' + this.serviceFilter + '" />');
      }
      if (this.statusFilter > -1) {
        query.push('<eq field="status.id" type="Assignment" value="' + this.statusFilter + '" />');
      }
      query.push("</and></base-query></query>");

      var queryStr = query.join("");

      return queryStr;
    }

    resetStatus(id) {
      this.ready("se.infomaker.dashboard-agent-plugin", () => {
        this.send("newspilot:makeRequest", {
          reqid: Dashboard.createUUID(),
          method: "PUT",
          url: "/newspilot/rest/entities/Assignment/" + id,
          body: '{"status": 100}'
        });
      });
    }
  }

  Dashboard.register({
    bundle: "@plugin_bundle",
    name: "@plugin_name",
    author: "@plugin_author",
    graphic_url: "@graphic_url",
    version: "@plugin_version",
    application: Application
  });
})(window.Dashboard, window.React);
