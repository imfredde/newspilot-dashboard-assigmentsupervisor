export default {
  Ad: {
    adClass: {
      propertyName: "adClass",
      propertyKey: "adClass.id",
      valueKey: "ad_class_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "AdClass"
    },
    adType: {
      propertyName: "adType",
      propertyKey: "adType",
      valueKey: "ad_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    approved: {
      propertyName: "approved",
      propertyKey: "approved",
      valueKey: "approved",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    approvedPage: {
      propertyName: "approvedPage",
      propertyKey: "approvedPage.id",
      valueKey: "approved_page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    approvedX1: {
      propertyName: "approvedX1",
      propertyKey: "approvedX1",
      valueKey: "approved_x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedX2: {
      propertyName: "approvedX2",
      propertyKey: "approvedX2",
      valueKey: "approved_x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedXOffset: {
      propertyName: "approvedXOffset",
      propertyKey: "approvedXOffset",
      valueKey: "approved_x_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedY1: {
      propertyName: "approvedY1",
      propertyKey: "approvedY1",
      valueKey: "approved_y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedY2: {
      propertyName: "approvedY2",
      propertyKey: "approvedY2",
      valueKey: "approved_y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedYOffset: {
      propertyName: "approvedYOffset",
      propertyKey: "approvedYOffset",
      valueKey: "approved_y_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    bleed: {
      propertyName: "bleed",
      propertyKey: "bleed",
      valueKey: "bleed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    color: {
      propertyName: "color",
      propertyKey: "color",
      valueKey: "color",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    columns: {
      propertyName: "columns",
      propertyKey: "columns",
      valueKey: "columns",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    currentPage: {
      propertyName: "currentPage",
      propertyKey: "currentPage.id",
      valueKey: "current_page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    currentPublications: {
      propertyName: "currentPublications",
      propertyKey: "currentPublications",
      valueKey: "current_publications",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    currentX1: {
      propertyName: "currentX1",
      propertyKey: "currentX1",
      valueKey: "current_x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentX2: {
      propertyName: "currentX2",
      propertyKey: "currentX2",
      valueKey: "current_x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentXOffset: {
      propertyName: "currentXOffset",
      propertyKey: "currentXOffset",
      valueKey: "current_x_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentY1: {
      propertyName: "currentY1",
      propertyKey: "currentY1",
      valueKey: "current_y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentY2: {
      propertyName: "currentY2",
      propertyKey: "currentY2",
      valueKey: "current_y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentYOffset: {
      propertyName: "currentYOffset",
      propertyKey: "currentYOffset",
      valueKey: "current_y_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    custom1: {
      propertyName: "custom1",
      propertyKey: "custom1",
      valueKey: "custom1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom2: {
      propertyName: "custom2",
      propertyKey: "custom2",
      valueKey: "custom2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom3: {
      propertyName: "custom3",
      propertyKey: "custom3",
      valueKey: "custom3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom4: {
      propertyName: "custom4",
      propertyKey: "custom4",
      valueKey: "custom4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom5: {
      propertyName: "custom5",
      propertyKey: "custom5",
      valueKey: "custom5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom6: {
      propertyName: "custom6",
      propertyKey: "custom6",
      valueKey: "custom6",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom7: {
      propertyName: "custom7",
      propertyKey: "custom7",
      valueKey: "custom7",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom8: {
      propertyName: "custom8",
      propertyKey: "custom8",
      valueKey: "custom8",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom9: {
      propertyName: "custom9",
      propertyKey: "custom9",
      valueKey: "custom9",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    customer: {
      propertyName: "customer",
      propertyKey: "customer",
      valueKey: "customer",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    disabled: {
      propertyName: "disabled",
      propertyKey: "disabled",
      valueKey: "disabled",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    dummy: {
      propertyName: "dummy",
      propertyKey: "dummy",
      valueKey: "dummy",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    edition: {
      propertyName: "edition",
      propertyKey: "edition",
      valueKey: "edition",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    firstDate: {
      propertyName: "firstDate",
      propertyKey: "firstDate",
      valueKey: "first_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lastDate: {
      propertyName: "lastDate",
      propertyKey: "lastDate",
      valueKey: "last_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    latestDate: {
      propertyName: "latestDate",
      propertyKey: "latestDate",
      valueKey: "latest_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    materialId: {
      propertyName: "materialId",
      propertyKey: "materialId",
      valueKey: "material_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    maxPublications: {
      propertyName: "maxPublications",
      propertyKey: "maxPublications",
      valueKey: "max_publications",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    originalValues: {
      propertyName: "originalValues",
      propertyKey: "originalValues",
      valueKey: "original_values",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    page: {
      propertyName: "page",
      propertyKey: "page.id",
      valueKey: "page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    path: {
      propertyName: "path",
      propertyKey: "path",
      valueKey: "path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    payingCustomer: {
      propertyName: "payingCustomer",
      propertyKey: "payingCustomer",
      valueKey: "paying_customer",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    placementCode: {
      propertyName: "placementCode",
      propertyKey: "placementCode",
      valueKey: "placement_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    previewDate: {
      propertyName: "previewDate",
      propertyKey: "previewDate",
      valueKey: "preview_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    publicationDate: {
      propertyName: "publicationDate",
      propertyKey: "publicationDate.id",
      valueKey: "publication_date_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PublicationDate"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    standby: {
      propertyName: "standby",
      propertyKey: "standby",
      valueKey: "standby",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    url: {
      propertyName: "url",
      propertyKey: "url",
      valueKey: "url",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    usageRemoved: {
      propertyName: "usageRemoved",
      propertyKey: "usageRemoved",
      valueKey: "usage_removed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    usedPlacementCode: {
      propertyName: "usedPlacementCode",
      propertyKey: "usedPlacementCode",
      valueKey: "used_placement_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    x1: {
      propertyName: "x1",
      propertyKey: "x1",
      valueKey: "x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    x2: {
      propertyName: "x2",
      propertyKey: "x2",
      valueKey: "x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    xOffset: {
      propertyName: "xOffset",
      propertyKey: "xOffset",
      valueKey: "x_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    y1: {
      propertyName: "y1",
      propertyKey: "y1",
      valueKey: "y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    y2: {
      propertyName: "y2",
      propertyKey: "y2",
      valueKey: "y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    yOffset: {
      propertyName: "yOffset",
      propertyKey: "yOffset",
      valueKey: "y_offset",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    }
  },
  AdClass: {
    allowResize: {
      propertyName: "allowResize",
      propertyKey: "allowResize",
      valueKey: "allow_resize",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    children: {
      propertyName: "children",
      propertyKey: "children",
      valueKey: "children_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "AdClass"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    hasHeader: {
      propertyName: "hasHeader",
      propertyKey: "hasHeader",
      valueKey: "has_header",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    headerLineProfile: {
      propertyName: "headerLineProfile",
      propertyKey: "headerLineProfile.id",
      valueKey: "header_line_profile_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "LineProfile"
    },
    headerOnNewPage: {
      propertyName: "headerOnNewPage",
      propertyKey: "headerOnNewPage",
      valueKey: "header_on_new_page",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lineProfile: {
      propertyName: "lineProfile",
      propertyKey: "lineProfile.id",
      valueKey: "line_profile_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "LineProfile"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    objectStyle: {
      propertyName: "objectStyle",
      propertyKey: "objectStyle",
      valueKey: "object_style",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parent: {
      propertyName: "parent",
      propertyKey: "parent.id",
      valueKey: "parent_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "AdClass"
    },
    placementPolicy: {
      propertyName: "placementPolicy",
      propertyKey: "placementPolicy",
      valueKey: "placement_policy",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    sortInherited: {
      propertyName: "sortInherited",
      propertyKey: "sortInherited",
      valueKey: "sort_inherited",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortKey: {
      propertyName: "sortKey",
      propertyKey: "sortKey",
      valueKey: "sort_key",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    sortLevel1Ascending: {
      propertyName: "sortLevel1Ascending",
      propertyKey: "sortLevel1Ascending",
      valueKey: "sort_level_1_ascending",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortLevel1Name: {
      propertyName: "sortLevel1Name",
      propertyKey: "sortLevel1Name",
      valueKey: "sort_level_1_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sortLevel2Ascending: {
      propertyName: "sortLevel2Ascending",
      propertyKey: "sortLevel2Ascending",
      valueKey: "sort_level_2_ascending",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortLevel2Name: {
      propertyName: "sortLevel2Name",
      propertyKey: "sortLevel2Name",
      valueKey: "sort_level_2_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sortLevel3Ascending: {
      propertyName: "sortLevel3Ascending",
      propertyKey: "sortLevel3Ascending",
      valueKey: "sort_level_3_ascending",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortLevel3Name: {
      propertyName: "sortLevel3Name",
      propertyKey: "sortLevel3Name",
      valueKey: "sort_level_3_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    spaceAbove: {
      propertyName: "spaceAbove",
      propertyKey: "spaceAbove",
      valueKey: "space_above",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    spaceBelow: {
      propertyName: "spaceBelow",
      propertyKey: "spaceBelow",
      valueKey: "space_below",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  AdHeader: {
    adClass: {
      propertyName: "adClass",
      propertyKey: "adClass.id",
      valueKey: "ad_class_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "AdClass"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    height: {
      propertyName: "height",
      propertyKey: "height",
      valueKey: "height",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    path: {
      propertyName: "path",
      propertyKey: "path",
      valueKey: "path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    spaceAbove: {
      propertyName: "spaceAbove",
      propertyKey: "spaceAbove",
      valueKey: "space_above",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    spaceBelow: {
      propertyName: "spaceBelow",
      propertyKey: "spaceBelow",
      valueKey: "space_below",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    width: {
      propertyName: "width",
      propertyKey: "width",
      valueKey: "width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    }
  },
  AdminLock: {
    groupId: {
      propertyName: "groupId",
      propertyKey: "groupId",
      valueKey: "group_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    session: {
      propertyName: "session",
      propertyKey: "session.id",
      valueKey: "session_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserSessionInfo"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  AdminPrivilege: {
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  Article: {
    archiveId: {
      propertyName: "archiveId",
      propertyKey: "archiveId",
      valueKey: "archive_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    articleLinkReferences: {
      propertyName: "articleLinkReferences",
      propertyKey: "articleLinkReferences",
      valueKey: "article_link_reference_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticleLinkReference"
    },
    articleParts: {
      propertyName: "articleParts",
      propertyKey: "articleParts",
      valueKey: "article_part_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticlePart"
    },
    articleType: {
      propertyName: "articleType",
      propertyKey: "articleType.id",
      valueKey: "article_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleType"
    },
    backgroundMaterial: {
      propertyName: "backgroundMaterial",
      propertyKey: "backgroundMaterial",
      valueKey: "background_material",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    backupVersion: {
      propertyName: "backupVersion",
      propertyKey: "backupVersion",
      valueKey: "backup_version",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    categories: {
      propertyName: "categories",
      propertyKey: "categories",
      valueKey: "categories",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    charSizeFactor: {
      propertyName: "charSizeFactor",
      propertyKey: "charSizeFactor",
      valueKey: "char_size_factor",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    coupledArticles: {
      propertyName: "coupledArticles",
      propertyKey: "coupledArticles",
      valueKey: "coupled_article_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Article"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataVersion: {
      propertyName: "dataVersion",
      propertyKey: "dataVersion",
      valueKey: "data_version",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    dirtyFlag: {
      propertyName: "dirtyFlag",
      propertyKey: "dirtyFlag",
      valueKey: "dirty_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    externalData: {
      propertyName: "externalData",
      propertyKey: "externalData",
      valueKey: "external_data",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    geodata: {
      propertyName: "geodata",
      propertyKey: "geodata",
      valueKey: "geodata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    idmlCRC: {
      propertyName: "idmlCRC",
      propertyKey: "idmlCRC",
      valueKey: "idml_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    idmlUpdatedDate: {
      propertyName: "idmlUpdatedDate",
      propertyKey: "idmlUpdatedDate",
      valueKey: "idml_updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    inheritLayout: {
      propertyName: "inheritLayout",
      propertyKey: "inheritLayout",
      valueKey: "inherit_layout",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    jobName: {
      propertyName: "jobName",
      propertyKey: "jobName",
      valueKey: "job_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutProperties: {
      propertyName: "layoutProperties",
      propertyKey: "layoutProperties",
      valueKey: "layout_properties",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    layoutStatus: {
      propertyName: "layoutStatus",
      propertyKey: "layoutStatus",
      valueKey: "layout_status",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutType: {
      propertyName: "layoutType",
      propertyKey: "layoutType",
      valueKey: "layout_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutTypeChanged: {
      propertyName: "layoutTypeChanged",
      propertyKey: "layoutTypeChanged",
      valueKey: "layout_type_changed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    masterArticle: {
      propertyName: "masterArticle",
      propertyKey: "masterArticle.id",
      valueKey: "master_article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    mediaId: {
      propertyName: "mediaId",
      propertyKey: "mediaId",
      valueKey: "media_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    microType: {
      propertyName: "microType",
      propertyKey: "microType",
      valueKey: "micro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    minorType: {
      propertyName: "minorType",
      propertyKey: "minorType",
      valueKey: "minor_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    newsMLVersion: {
      propertyName: "newsMLVersion",
      propertyKey: "newsMLVersion.id",
      valueKey: "newsml_version_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "NewsMLVersion"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    originalArticle: {
      propertyName: "originalArticle",
      propertyKey: "originalArticle.id",
      valueKey: "original_article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    page: {
      propertyName: "page",
      propertyKey: "page.id",
      valueKey: "page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    pageName: {
      propertyName: "pageName",
      propertyKey: "pageName",
      valueKey: "page_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    pageSpanCount: {
      propertyName: "pageSpanCount",
      propertyKey: "pageSpanCount",
      valueKey: "page_span_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    privateUser: {
      propertyName: "privateUser",
      propertyKey: "privateUser.id",
      valueKey: "private_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    pubStart: {
      propertyName: "pubStart",
      propertyKey: "pubStart",
      valueKey: "pub_start",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    pubStop: {
      propertyName: "pubStop",
      propertyKey: "pubStop",
      valueKey: "pub_stop",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    publicationDate: {
      propertyName: "publicationDate",
      propertyKey: "publicationDate.id",
      valueKey: "publication_date_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PublicationDate"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    respUser: {
      propertyName: "respUser",
      propertyKey: "respUser.id",
      valueKey: "resp_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    restored: {
      propertyName: "restored",
      propertyKey: "restored",
      valueKey: "restored_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    restoredFlag: {
      propertyName: "restoredFlag",
      propertyKey: "restoredFlag",
      valueKey: "restored_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    sections: {
      propertyName: "sections",
      propertyKey: "sections",
      valueKey: "sections",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Section"
    },
    slug: {
      propertyName: "slug",
      propertyKey: "slug",
      valueKey: "slug",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticleSystemLink"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    textlength: {
      propertyName: "textlength",
      propertyKey: "textlength",
      valueKey: "textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    textlengthMm: {
      propertyName: "textlengthMm",
      propertyKey: "textlengthMm",
      valueKey: "textlength_mm",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    urlLink: {
      propertyName: "urlLink",
      propertyKey: "urlLink",
      valueKey: "url_link",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    userdata: {
      propertyName: "userdata",
      propertyKey: "userdata",
      valueKey: "userdata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ArticleConfig: {
    allowed: {
      propertyName: "allowed",
      propertyKey: "allowed",
      valueKey: "allowed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    articlePartConfigs: {
      propertyName: "articlePartConfigs",
      propertyKey: "articlePartConfigs",
      valueKey: "article_part_config_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticlePartConfig"
    },
    articleType: {
      propertyName: "articleType",
      propertyKey: "articleType.id",
      valueKey: "article_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleType"
    },
    defaultIdmlTemplate: {
      propertyName: "defaultIdmlTemplate",
      propertyKey: "defaultIdmlTemplate.id",
      valueKey: "default_idml_template_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "IdmlTemplate"
    },
    fileName: {
      propertyName: "fileName",
      propertyKey: "fileName",
      valueKey: "file_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    idmlTemplates: {
      propertyName: "idmlTemplates",
      propertyKey: "idmlTemplates",
      valueKey: "idmltemplates",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "IdmlTemplate"
    },
    incopyDataSize: {
      propertyName: "incopyDataSize",
      propertyKey: "incopyDataSize",
      valueKey: "incopy_data_size",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutProperties: {
      propertyName: "layoutProperties",
      propertyKey: "layoutProperties",
      valueKey: "layout_properties",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    layoutStatus: {
      propertyName: "layoutStatus",
      propertyKey: "layoutStatus",
      valueKey: "layout_status",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    }
  },
  ArticleLinkReference: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    content: {
      propertyName: "content",
      propertyKey: "content",
      valueKey: "content",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    content_type: {
      propertyName: "content_type",
      propertyKey: "content_type",
      valueKey: "content_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internalRefId: {
      propertyName: "internalRefId",
      propertyKey: "internalRefId",
      valueKey: "internal_ref_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internalRefType: {
      propertyName: "internalRefType",
      propertyKey: "internalRefType",
      valueKey: "internal_ref_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    mimetype: {
      propertyName: "mimetype",
      propertyKey: "mimetype",
      valueKey: "mimetype",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    referenceType: {
      propertyName: "referenceType",
      propertyKey: "referenceType",
      valueKey: "reference_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    sortOrder: {
      propertyName: "sortOrder",
      propertyKey: "sortOrder",
      valueKey: "sort_order",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    uri: {
      propertyName: "uri",
      propertyKey: "uri",
      valueKey: "uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    uriCRC: {
      propertyName: "uriCRC",
      propertyKey: "uriCRC",
      valueKey: "uri_c_r_c",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    }
  },
  ArticlePart: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    articlePartType: {
      propertyName: "articlePartType",
      propertyKey: "articlePartType.id",
      valueKey: "article_part_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePartType"
    },
    charSizeFactor: {
      propertyName: "charSizeFactor",
      propertyKey: "charSizeFactor",
      valueKey: "char_size_factor",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageContainers: {
      propertyName: "imageContainers",
      propertyKey: "imageContainers",
      valueKey: "image_container_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageContainer"
    },
    layoutId: {
      propertyName: "layoutId",
      propertyKey: "layoutId",
      valueKey: "layout_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    newsMLVersion: {
      propertyName: "newsMLVersion",
      propertyKey: "newsMLVersion.id",
      valueKey: "newsml_version_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "NewsMLVersion"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sortkey",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticlePartSystemLink"
    },
    textlength: {
      propertyName: "textlength",
      propertyKey: "textlength",
      valueKey: "textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    textlengthMm: {
      propertyName: "textlengthMm",
      propertyKey: "textlengthMm",
      valueKey: "textlength_mm",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    totalTextLength: {
      propertyName: "totalTextLength",
      propertyKey: "totalTextLength",
      valueKey: "total_textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ArticlePartConfig: {
    allowed: {
      propertyName: "allowed",
      propertyKey: "allowed",
      valueKey: "allowed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    articleConfig: {
      propertyName: "articleConfig",
      propertyKey: "articleConfig.id",
      valueKey: "article_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleConfig"
    },
    articlePartType: {
      propertyName: "articlePartType",
      propertyKey: "articlePartType.id",
      valueKey: "article_part_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePartType"
    },
    defaultBylineType: {
      propertyName: "defaultBylineType",
      propertyKey: "defaultBylineType.id",
      valueKey: "default_byline_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "BylineType"
    },
    defaultPhotoBylineType: {
      propertyName: "defaultPhotoBylineType",
      propertyKey: "defaultPhotoBylineType.id",
      valueKey: "default_photo_byline_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "BylineType"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageCount: {
      propertyName: "imageCount",
      propertyKey: "imageCount",
      valueKey: "image_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    xmlElementConfig: {
      propertyName: "xmlElementConfig",
      propertyKey: "xmlElementConfig.id",
      valueKey: "xml_element_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "XmlElementConfig"
    }
  },
  ArticlePartSystemLink: {
    articlePart: {
      propertyName: "articlePart",
      propertyKey: "articlePart.id",
      valueKey: "article_part_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePart"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  ArticlePartType: {
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    export: {
      propertyName: "export",
      propertyKey: "export",
      valueKey: "export",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    forced: {
      propertyName: "forced",
      propertyKey: "forced",
      valueKey: "forced",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internal: {
      propertyName: "internal",
      propertyKey: "internal",
      valueKey: "internal",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sortkey",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  ArticleSystemLink: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  ArticleType: {
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    products: {
      propertyName: "products",
      propertyKey: "products",
      valueKey: "products",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Product"
    }
  },
  ArticleVersion: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    articleVersionType: {
      propertyName: "articleVersionType",
      propertyKey: "articleVersionType.id",
      valueKey: "article_version_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleVersionType"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutProperties: {
      propertyName: "layoutProperties",
      propertyKey: "layoutProperties",
      valueKey: "layout_properties",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mediaId: {
      propertyName: "mediaId",
      propertyKey: "mediaId",
      valueKey: "media_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    }
  },
  ArticleVersionType: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    locked: {
      propertyName: "locked",
      propertyKey: "locked",
      valueKey: "locked",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Assignment: {
    activationDate: {
      propertyName: "activationDate",
      propertyKey: "activationDate",
      valueKey: "activation_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    clientId: {
      propertyName: "clientId",
      propertyKey: "clientId",
      valueKey: "client_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    created: {
      propertyName: "created",
      propertyKey: "created",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    done: {
      propertyName: "done",
      propertyKey: "done",
      valueKey: "done",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    errormessage: {
      propertyName: "errormessage",
      propertyKey: "errormessage",
      valueKey: "errormessage",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    ip: {
      propertyName: "ip",
      propertyKey: "ip",
      valueKey: "ip",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    nodeId: {
      propertyName: "nodeId",
      propertyKey: "nodeId",
      valueKey: "node_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    nodeType: {
      propertyName: "nodeType",
      propertyKey: "nodeType",
      valueKey: "node_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    options: {
      propertyName: "options",
      propertyKey: "options",
      valueKey: "options",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    parameters: {
      propertyName: "parameters",
      propertyKey: "parameters",
      valueKey: "parameters",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    processedBy: {
      propertyName: "processedBy",
      propertyKey: "processedBy.id",
      valueKey: "processed_by",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    processtime: {
      propertyName: "processtime",
      propertyKey: "processtime",
      valueKey: "processtime",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    queuepos: {
      propertyName: "queuepos",
      propertyKey: "queuepos",
      valueKey: "queuepos",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    results: {
      propertyName: "results",
      propertyKey: "results",
      valueKey: "results",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    serviceWatch: {
      propertyName: "serviceWatch",
      propertyKey: "serviceWatch.id",
      valueKey: "servicewatch_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ServiceWatch"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.service.common.ServiceProviderType"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    workflowTrigger: {
      propertyName: "workflowTrigger",
      propertyKey: "workflowTrigger.id",
      valueKey: "workflow_trigger_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "WorkflowTrigger"
    }
  },
  Byline: {
    articlePart: {
      propertyName: "articlePart",
      propertyKey: "articlePart.id",
      valueKey: "article_part_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePart"
    },
    bylineType: {
      propertyName: "bylineType",
      propertyKey: "bylineType.id",
      valueKey: "byline_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "BylineType"
    },
    contentContainer: {
      propertyName: "contentContainer",
      propertyKey: "contentContainer.id",
      valueKey: "content_container_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ContentContainer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageContainer: {
      propertyName: "imageContainer",
      propertyKey: "imageContainer.id",
      valueKey: "image_container_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ImageContainer"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sortkey",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    userinfo: {
      propertyName: "userinfo",
      propertyKey: "userinfo",
      valueKey: "userinfo",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  BylineExport: {
    bylineCode: {
      propertyName: "bylineCode",
      propertyKey: "bylineCode",
      valueKey: "byline_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    bylineType: {
      propertyName: "bylineType",
      propertyKey: "bylineType.id",
      valueKey: "byline_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "BylineType"
    },
    exportConfig: {
      propertyName: "exportConfig",
      propertyKey: "exportConfig.id",
      valueKey: "export_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExportConfig"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    photoBylineCode: {
      propertyName: "photoBylineCode",
      propertyKey: "photoBylineCode",
      valueKey: "photo_byline_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  BylineType: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    }
  },
  CMSSyncMapping: {
    communicationService: {
      propertyName: "communicationService",
      propertyKey: "communicationService",
      valueKey: "communication_service",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    extraTargetIdentifier: {
      propertyName: "extraTargetIdentifier",
      propertyKey: "extraTargetIdentifier",
      valueKey: "extra_target_identifier",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    idPath: {
      propertyName: "idPath",
      propertyKey: "idPath",
      valueKey: "id_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    methodName: {
      propertyName: "methodName",
      propertyKey: "methodName",
      valueKey: "method_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parameterType1: {
      propertyName: "parameterType1",
      propertyKey: "parameterType1",
      valueKey: "parameter_type_1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    parameterType2: {
      propertyName: "parameterType2",
      propertyKey: "parameterType2",
      valueKey: "parameter_type_2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    parameterType3: {
      propertyName: "parameterType3",
      propertyKey: "parameterType3",
      valueKey: "parameter_type_3",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    parameterType4: {
      propertyName: "parameterType4",
      propertyKey: "parameterType4",
      valueKey: "parameter_type_4",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    parameterType5: {
      propertyName: "parameterType5",
      propertyKey: "parameterType5",
      valueKey: "parameter_type_5",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    parameterValue1: {
      propertyName: "parameterValue1",
      propertyKey: "parameterValue1",
      valueKey: "parameter_value_1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parameterValue2: {
      propertyName: "parameterValue2",
      propertyKey: "parameterValue2",
      valueKey: "parameter_value_2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parameterValue3: {
      propertyName: "parameterValue3",
      propertyKey: "parameterValue3",
      valueKey: "parameter_value_3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parameterValue4: {
      propertyName: "parameterValue4",
      propertyKey: "parameterValue4",
      valueKey: "parameter_value_4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parameterValue5: {
      propertyName: "parameterValue5",
      propertyKey: "parameterValue5",
      valueKey: "parameter_value_5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parentPath: {
      propertyName: "parentPath",
      propertyKey: "parentPath",
      valueKey: "parent_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    path: {
      propertyName: "path",
      propertyKey: "path",
      valueKey: "path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    returnType: {
      propertyName: "returnType",
      propertyKey: "returnType",
      valueKey: "return_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    structurePath: {
      propertyName: "structurePath",
      propertyKey: "structurePath",
      valueKey: "structure_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    synchronizedDate: {
      propertyName: "synchronizedDate",
      propertyKey: "synchronizedDate",
      valueKey: "synchronized_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    targetField: {
      propertyName: "targetField",
      propertyKey: "targetField",
      valueKey: "target_field",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    valuePath: {
      propertyName: "valuePath",
      propertyKey: "valuePath",
      valueKey: "value_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  CalendarEvent: {
    approved: {
      propertyName: "approved",
      propertyKey: "approved",
      valueKey: "approved",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    calendarEventType: {
      propertyName: "calendarEventType",
      propertyKey: "calendarEventType.id",
      valueKey: "calendar_event_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "CalendarEventType"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    end: {
      propertyName: "end",
      propertyKey: "end",
      valueKey: "end_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    eventType: {
      propertyName: "eventType",
      propertyKey: "eventType",
      valueKey: "event_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    ownerUser: {
      propertyName: "ownerUser",
      propertyKey: "ownerUser.id",
      valueKey: "owner_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    resourceSchedule: {
      propertyName: "resourceSchedule",
      propertyKey: "resourceSchedule.id",
      valueKey: "resource_schedule_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ResourceSchedule"
    },
    resourceStatus: {
      propertyName: "resourceStatus",
      propertyKey: "resourceStatus.id",
      valueKey: "resource_status_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ResourceStatus"
    },
    start: {
      propertyName: "start",
      propertyKey: "start",
      valueKey: "start_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    summary: {
      propertyName: "summary",
      propertyKey: "summary",
      valueKey: "summary",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    totalTime: {
      propertyName: "totalTime",
      propertyKey: "totalTime",
      valueKey: "total_time",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    userRole: {
      propertyName: "userRole",
      propertyKey: "userRole.id",
      valueKey: "user_role_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserRole"
    },
    users: {
      propertyName: "users",
      propertyKey: "users",
      valueKey: "users",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "User"
    }
  },
  CalendarEventType: {
    end: {
      propertyName: "end",
      propertyKey: "end",
      valueKey: "end",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    start: {
      propertyName: "start",
      propertyKey: "start",
      valueKey: "start",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    time: {
      propertyName: "time",
      propertyKey: "time",
      valueKey: "time",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    userRole: {
      propertyName: "userRole",
      propertyKey: "userRole.id",
      valueKey: "user_role_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserRole"
    }
  },
  CalendarPeriod: {
    calendarDate: {
      propertyName: "calendarDate",
      propertyKey: "calendarDate",
      valueKey: "date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    timeEnd: {
      propertyName: "timeEnd",
      propertyKey: "timeEnd",
      valueKey: "time_end",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    timeStart: {
      propertyName: "timeStart",
      propertyKey: "timeStart",
      valueKey: "time_start",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    weekdays: {
      propertyName: "weekdays",
      propertyKey: "weekdays",
      valueKey: "weekdays",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  CharacterExport: {
    exportCode: {
      propertyName: "exportCode",
      propertyKey: "exportCode",
      valueKey: "export_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    exportConfig: {
      propertyName: "exportConfig",
      propertyKey: "exportConfig.id",
      valueKey: "export_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExportConfig"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    specialChar: {
      propertyName: "specialChar",
      propertyKey: "specialChar.id",
      valueKey: "special_char_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SpecialChar"
    },
    unicode: {
      propertyName: "unicode",
      propertyKey: "unicode",
      valueKey: "unicode",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  Contact: {
    address: {
      propertyName: "address",
      propertyKey: "address",
      valueKey: "address",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    city: {
      propertyName: "city",
      propertyKey: "city",
      valueKey: "city",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    company: {
      propertyName: "company",
      propertyKey: "company",
      valueKey: "company",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    contactAssociations: {
      propertyName: "contactAssociations",
      propertyKey: "contactAssociations",
      valueKey: "contact_associations",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContactAssociation"
    },
    contactType: {
      propertyName: "contactType",
      propertyKey: "contactType",
      valueKey: "contact_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    contacts: {
      propertyName: "contacts",
      propertyKey: "contacts",
      valueKey: "contacts",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    country: {
      propertyName: "country",
      propertyKey: "country",
      valueKey: "country",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    email: {
      propertyName: "email",
      propertyKey: "email",
      valueKey: "email",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    family: {
      propertyName: "family",
      propertyKey: "family",
      valueKey: "family",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    homePage: {
      propertyName: "homePage",
      propertyKey: "homePage",
      valueKey: "home_page",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    keywords: {
      propertyName: "keywords",
      propertyKey: "keywords",
      valueKey: "keywords",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    latitude: {
      propertyName: "latitude",
      propertyKey: "latitude",
      valueKey: "latitude",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    longitude: {
      propertyName: "longitude",
      propertyKey: "longitude",
      valueKey: "longitude",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    phoneExtra1: {
      propertyName: "phoneExtra1",
      propertyKey: "phoneExtra1",
      valueKey: "phone_extra_1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    phoneExtra2: {
      propertyName: "phoneExtra2",
      propertyKey: "phoneExtra2",
      valueKey: "phone_extra_2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    phoneHome: {
      propertyName: "phoneHome",
      propertyKey: "phoneHome",
      valueKey: "phone_home",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    phoneMobile: {
      propertyName: "phoneMobile",
      propertyKey: "phoneMobile",
      valueKey: "phone_mobile",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    phoneWork: {
      propertyName: "phoneWork",
      propertyKey: "phoneWork",
      valueKey: "phone_work",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    privateUser: {
      propertyName: "privateUser",
      propertyKey: "privateUser.id",
      valueKey: "private_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    reference: {
      propertyName: "reference",
      propertyKey: "reference",
      valueKey: "reference",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    ssn: {
      propertyName: "ssn",
      propertyKey: "ssn",
      valueKey: "ssn",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    title: {
      propertyName: "title",
      propertyKey: "title",
      valueKey: "title",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    zipCode: {
      propertyName: "zipCode",
      propertyKey: "zipCode",
      valueKey: "zip_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ContactAssociation: {
    associatedContact: {
      propertyName: "associatedContact",
      propertyKey: "associatedContact.id",
      valueKey: "associated_contact_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Contact"
    },
    associationType: {
      propertyName: "associationType",
      propertyKey: "associationType",
      valueKey: "association_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    contact: {
      propertyName: "contact",
      propertyKey: "contact.id",
      valueKey: "contact_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Contact"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  Content: {
    archiveId: {
      propertyName: "archiveId",
      propertyKey: "archiveId",
      valueKey: "archive_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    author: {
      propertyName: "author",
      propertyKey: "author.id",
      valueKey: "author_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    authorName: {
      propertyName: "authorName",
      propertyKey: "authorName",
      valueKey: "author_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    backgroundMaterial: {
      propertyName: "backgroundMaterial",
      propertyKey: "backgroundMaterial",
      valueKey: "background_material",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    byteSize: {
      propertyName: "byteSize",
      propertyKey: "byteSize",
      valueKey: "byte_size",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    captionProposed: {
      propertyName: "captionProposed",
      propertyKey: "captionProposed",
      valueKey: "caption_proposed",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    categories: {
      propertyName: "categories",
      propertyKey: "categories",
      valueKey: "categories",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    channelId: {
      propertyName: "channelId",
      propertyKey: "channelId",
      valueKey: "channel_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    contentLinks: {
      propertyName: "contentLinks",
      propertyKey: "contentLinks",
      valueKey: "contentlinks",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentLink"
    },
    contentType: {
      propertyName: "contentType",
      propertyKey: "contentType",
      valueKey: "content_type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.Content.ContentType"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    externalMetadata: {
      propertyName: "externalMetadata",
      propertyKey: "externalMetadata",
      valueKey: "external_metadata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    fileCreator: {
      propertyName: "fileCreator",
      propertyKey: "fileCreator",
      valueKey: "file_creator",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    fileExt: {
      propertyName: "fileExt",
      propertyKey: "fileExt",
      valueKey: "file_ext",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    fileType: {
      propertyName: "fileType",
      propertyKey: "fileType",
      valueKey: "file_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    metadata: {
      propertyName: "metadata",
      propertyKey: "metadata",
      valueKey: "metadata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mimetype: {
      propertyName: "mimetype",
      propertyKey: "mimetype",
      valueKey: "mimetype",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    noArchiving: {
      propertyName: "noArchiving",
      propertyKey: "noArchiving",
      valueKey: "no_archiving",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    originalId: {
      propertyName: "originalId",
      propertyKey: "originalId",
      valueKey: "original_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    originalRemoved: {
      propertyName: "originalRemoved",
      propertyKey: "originalRemoved",
      valueKey: "original_removed",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    placeable: {
      propertyName: "placeable",
      propertyKey: "placeable",
      valueKey: "placeable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    production: {
      propertyName: "production",
      propertyKey: "production",
      valueKey: "production",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    reproHandler: {
      propertyName: "reproHandler",
      propertyKey: "reproHandler.id",
      valueKey: "repro_handler_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ReproHandler"
    },
    reproType: {
      propertyName: "reproType",
      propertyKey: "reproType",
      valueKey: "repro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    storeLocation: {
      propertyName: "storeLocation",
      propertyKey: "storeLocation.id",
      valueKey: "storelocation_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "StoreLocation"
    },
    storePath: {
      propertyName: "storePath",
      propertyKey: "storePath",
      valueKey: "store_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentSystemLink"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ContentContainer: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    articlePart: {
      propertyName: "articlePart",
      propertyKey: "articlePart.id",
      valueKey: "article_part_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePart"
    },
    bylines: {
      propertyName: "bylines",
      propertyKey: "bylines",
      valueKey: "byline_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Byline"
    },
    contentLink: {
      propertyName: "contentLink",
      propertyKey: "contentLink.id",
      valueKey: "contentlink_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ContentLink"
    },
    content_data: {
      propertyName: "content_data",
      propertyKey: "content_data",
      valueKey: "content_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "Content"
    },
    contentlink_data: {
      propertyName: "contentlink_data",
      propertyKey: "contentlink_data",
      valueKey: "contentlink_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "ContentLink"
    },
    datacrc: {
      propertyName: "datacrc",
      propertyKey: "datacrc",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutId: {
      propertyName: "layoutId",
      propertyKey: "layoutId",
      valueKey: "layout_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    newsMLVersion: {
      propertyName: "newsMLVersion",
      propertyKey: "newsMLVersion.id",
      valueKey: "newsml_version_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "NewsMLVersion"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    respUser: {
      propertyName: "respUser",
      propertyKey: "respUser.id",
      valueKey: "resp_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sortkey",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentContainerSystemLink"
    },
    textlength: {
      propertyName: "textlength",
      propertyKey: "textlength",
      valueKey: "textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    totalTextlength: {
      propertyName: "totalTextlength",
      propertyKey: "totalTextlength",
      valueKey: "total_textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ContentContainerSystemLink: {
    contentContainer: {
      propertyName: "contentContainer",
      propertyKey: "contentContainer.id",
      valueKey: "content_container_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ContentContainer"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  ContentLink: {
    content: {
      propertyName: "content",
      propertyKey: "content.id",
      valueKey: "content_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Content"
    },
    contentContainers: {
      propertyName: "contentContainers",
      propertyKey: "contentContainers",
      valueKey: "content_container_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentContainer"
    },
    content_data: {
      propertyName: "content_data",
      propertyKey: "content_data",
      valueKey: "content_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "Content"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  ContentSystemLink: {
    content: {
      propertyName: "content",
      propertyKey: "content.id",
      valueKey: "content_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Content"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  Department: {
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    jobPrio: {
      propertyName: "jobPrio",
      propertyKey: "jobPrio",
      valueKey: "job_prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    jobStatus: {
      propertyName: "jobStatus",
      propertyKey: "jobStatus",
      valueKey: "job_status",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    jobStatusAdvanced: {
      propertyName: "jobStatusAdvanced",
      propertyKey: "jobStatusAdvanced",
      valueKey: "job_status_advanced",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    users: {
      propertyName: "users",
      propertyKey: "users",
      valueKey: "user_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "User"
    }
  },
  EntityFlag: {
    fillSpace: {
      propertyName: "fillSpace",
      propertyKey: "fillSpace",
      valueKey: "fill_space",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    locked: {
      propertyName: "locked",
      propertyKey: "locked",
      valueKey: "locked",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    offSymbol: {
      propertyName: "offSymbol",
      propertyKey: "offSymbol",
      valueKey: "off_symbol",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    onSymbol: {
      propertyName: "onSymbol",
      propertyKey: "onSymbol",
      valueKey: "on_symbol",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    sortKey: {
      propertyName: "sortKey",
      propertyKey: "sortKey",
      valueKey: "sort_key",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.NPEntityType"
    },
    userEditable: {
      propertyName: "userEditable",
      propertyKey: "userEditable",
      valueKey: "user_editable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  EntityLock: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lockedArticles: {
      propertyName: "lockedArticles",
      propertyKey: "lockedArticles",
      valueKey: "locked_article_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Article"
    },
    lockedBy: {
      propertyName: "lockedBy",
      propertyKey: "lockedBy",
      valueKey: "locked_by",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    lockedContentContainers: {
      propertyName: "lockedContentContainers",
      propertyKey: "lockedContentContainers",
      valueKey: "locked_content_container_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentContainer"
    },
    lockedContents: {
      propertyName: "lockedContents",
      propertyKey: "lockedContents",
      valueKey: "locked_content_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Content"
    },
    lockedImageContainers: {
      propertyName: "lockedImageContainers",
      propertyKey: "lockedImageContainers",
      valueKey: "locked_image_container_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageContainer"
    },
    lockedImages: {
      propertyName: "lockedImages",
      propertyKey: "lockedImages",
      valueKey: "locked_image_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Image"
    },
    lockedJobs: {
      propertyName: "lockedJobs",
      propertyKey: "lockedJobs",
      valueKey: "locked_job_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Job"
    },
    lockedPages: {
      propertyName: "lockedPages",
      propertyKey: "lockedPages",
      valueKey: "locked_page_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Page"
    },
    session: {
      propertyName: "session",
      propertyKey: "session.id",
      valueKey: "session_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserSessionInfo"
    },
    token: {
      propertyName: "token",
      propertyKey: "token",
      valueKey: "token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  EntityTemplate: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    defaultName: {
      propertyName: "defaultName",
      propertyKey: "defaultName",
      valueKey: "default_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    targetEntityType: {
      propertyName: "targetEntityType",
      propertyKey: "targetEntityType",
      valueKey: "target_entity_type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.NPEntityType"
    }
  },
  ExportConfig: {
    articleConfig: {
      propertyName: "articleConfig",
      propertyKey: "articleConfig.id",
      valueKey: "article_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleConfig"
    },
    articlePartType: {
      propertyName: "articlePartType",
      propertyKey: "articlePartType.id",
      valueKey: "article_part_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePartType"
    },
    bylineExports: {
      propertyName: "bylineExports",
      propertyKey: "bylineExports",
      valueKey: "byline_export_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "BylineExport"
    },
    characterExports: {
      propertyName: "characterExports",
      propertyKey: "characterExports",
      valueKey: "character_export_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "CharacterExport"
    },
    configdata: {
      propertyName: "configdata",
      propertyKey: "configdata",
      valueKey: "configdata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    exportPlugin: {
      propertyName: "exportPlugin",
      propertyKey: "exportPlugin.id",
      valueKey: "export_plugin_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExportPlugin"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    xmlElementExports: {
      propertyName: "xmlElementExports",
      propertyKey: "xmlElementExports",
      valueKey: "xml_element_export_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "XmlElementExport"
    }
  },
  ExportDestination: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    defaultImageWidth: {
      propertyName: "defaultImageWidth",
      propertyKey: "defaultImageWidth",
      valueKey: "default_image_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageContainerSortByPrio: {
      propertyName: "imageContainerSortByPrio",
      propertyKey: "imageContainerSortByPrio",
      valueKey: "image_container_sort_by_prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    imageExportOriginal: {
      propertyName: "imageExportOriginal",
      propertyKey: "imageExportOriginal",
      valueKey: "image_export_original",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    passwd: {
      propertyName: "passwd",
      propertyKey: "passwd",
      valueKey: "passwd",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    path: {
      propertyName: "path",
      propertyKey: "path",
      valueKey: "path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    port: {
      propertyName: "port",
      propertyKey: "port",
      valueKey: "port",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    previewPath: {
      propertyName: "previewPath",
      propertyKey: "previewPath",
      valueKey: "preview_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    server: {
      propertyName: "server",
      propertyKey: "server",
      valueKey: "server",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    userId: {
      propertyName: "userId",
      propertyKey: "userId",
      valueKey: "user_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ExportPlugin: {
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    exportType: {
      propertyName: "exportType",
      propertyKey: "exportType",
      valueKey: "export_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    media: {
      propertyName: "media",
      propertyKey: "media",
      valueKey: "media",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.Media"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    publicationConfig: {
      propertyName: "publicationConfig",
      propertyKey: "publicationConfig",
      valueKey: "publication_config",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    versionCode: {
      propertyName: "versionCode",
      propertyKey: "versionCode",
      valueKey: "version_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ExternalSystem: {
    clientImport: {
      propertyName: "clientImport",
      propertyKey: "clientImport",
      valueKey: "client_import",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    dataRequest: {
      propertyName: "dataRequest",
      propertyKey: "dataRequest",
      valueKey: "data_request",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    defaultNpType: {
      propertyName: "defaultNpType",
      propertyKey: "defaultNpType",
      valueKey: "default_np_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    host: {
      propertyName: "host",
      propertyKey: "host",
      valueKey: "host",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    idField: {
      propertyName: "idField",
      propertyKey: "idField",
      valueKey: "id_field",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    idFieldSep: {
      propertyName: "idFieldSep",
      propertyKey: "idFieldSep",
      valueKey: "id_field_sep",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    metadataRequest: {
      propertyName: "metadataRequest",
      propertyKey: "metadataRequest",
      valueKey: "metadata_request",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mimetypeField: {
      propertyName: "mimetypeField",
      propertyKey: "mimetypeField",
      valueKey: "mimetype_field",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mimetypeFieldSep: {
      propertyName: "mimetypeFieldSep",
      propertyKey: "mimetypeFieldSep",
      valueKey: "mimetype_field_sep",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    nameField: {
      propertyName: "nameField",
      propertyKey: "nameField",
      valueKey: "name_field",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    nameFieldSep: {
      propertyName: "nameFieldSep",
      propertyKey: "nameFieldSep",
      valueKey: "name_field_sep",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    password: {
      propertyName: "password",
      propertyKey: "password",
      valueKey: "password",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    port: {
      propertyName: "port",
      propertyKey: "port",
      valueKey: "port",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    previewRequest: {
      propertyName: "previewRequest",
      propertyKey: "previewRequest",
      valueKey: "preview_request",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    protocol: {
      propertyName: "protocol",
      propertyKey: "protocol",
      valueKey: "protocol",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    systemType: {
      propertyName: "systemType",
      propertyKey: "systemType",
      valueKey: "system_type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.ExternalSystem.SystemType"
    },
    thumbRequest: {
      propertyName: "thumbRequest",
      propertyKey: "thumbRequest",
      valueKey: "thumb_request",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    userField: {
      propertyName: "userField",
      propertyKey: "userField",
      valueKey: "user_field",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    userFieldSep: {
      propertyName: "userFieldSep",
      propertyKey: "userFieldSep",
      valueKey: "user_field_sep",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    userName: {
      propertyName: "userName",
      propertyKey: "userName",
      valueKey: "user_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ExternalSystemSetting: {
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    key: {
      propertyName: "key",
      propertyKey: "key",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    value: {
      propertyName: "value",
      propertyKey: "value",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  FileManagerItem: {
    deleted: {
      propertyName: "deleted",
      propertyKey: "deleted",
      valueKey: "deleted",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    filename: {
      propertyName: "filename",
      propertyKey: "filename",
      valueKey: "filename",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    path: {
      propertyName: "path",
      propertyKey: "path",
      valueKey: "path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  History: {
    action: {
      propertyName: "action",
      propertyKey: "action",
      valueKey: "action",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    created: {
      propertyName: "created",
      propertyKey: "created",
      valueKey: "created",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    elapsed: {
      propertyName: "elapsed",
      propertyKey: "elapsed",
      valueKey: "elapsed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    organizationId: {
      propertyName: "organizationId",
      propertyKey: "organizationId",
      valueKey: "organization_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    refId: {
      propertyName: "refId",
      propertyKey: "refId",
      valueKey: "ref_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    refName: {
      propertyName: "refName",
      propertyKey: "refName",
      valueKey: "ref_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    refType: {
      propertyName: "refType",
      propertyKey: "refType",
      valueKey: "ref_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    userId: {
      propertyName: "userId",
      propertyKey: "userId",
      valueKey: "user_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  IdmlTemplate: {
    articleConfigs: {
      propertyName: "articleConfigs",
      propertyKey: "articleConfigs",
      valueKey: "article_config_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticleConfig"
    },
    articleType: {
      propertyName: "articleType",
      propertyKey: "articleType.id",
      valueKey: "article_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleType"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  Image: {
    archiveId: {
      propertyName: "archiveId",
      propertyKey: "archiveId",
      valueKey: "archive_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    backgroundMaterial: {
      propertyName: "backgroundMaterial",
      propertyKey: "backgroundMaterial",
      valueKey: "background_material",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    backupCRC: {
      propertyName: "backupCRC",
      propertyKey: "backupCRC",
      valueKey: "backup_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    byteSize: {
      propertyName: "byteSize",
      propertyKey: "byteSize",
      valueKey: "byte_size",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    captionProposed: {
      propertyName: "captionProposed",
      propertyKey: "captionProposed",
      valueKey: "caption_proposed",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    categories: {
      propertyName: "categories",
      propertyKey: "categories",
      valueKey: "categories",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    channelId: {
      propertyName: "channelId",
      propertyKey: "channelId",
      valueKey: "channel_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    city: {
      propertyName: "city",
      propertyKey: "city",
      valueKey: "city",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    colorspace: {
      propertyName: "colorspace",
      propertyKey: "colorspace",
      valueKey: "colorspace",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    copyright: {
      propertyName: "copyright",
      propertyKey: "copyright",
      valueKey: "copyright",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    country: {
      propertyName: "country",
      propertyKey: "country",
      valueKey: "country",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    cropExists: {
      propertyName: "cropExists",
      propertyKey: "cropExists",
      valueKey: "crop_exists",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    cropProfile: {
      propertyName: "cropProfile",
      propertyKey: "cropProfile",
      valueKey: "crop_profile",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom1: {
      propertyName: "custom1",
      propertyKey: "custom1",
      valueKey: "custom1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom2: {
      propertyName: "custom2",
      propertyKey: "custom2",
      valueKey: "custom2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom3: {
      propertyName: "custom3",
      propertyKey: "custom3",
      valueKey: "custom3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom4: {
      propertyName: "custom4",
      propertyKey: "custom4",
      valueKey: "custom4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom5: {
      propertyName: "custom5",
      propertyKey: "custom5",
      valueKey: "custom5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom6: {
      propertyName: "custom6",
      propertyKey: "custom6",
      valueKey: "custom6",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom7: {
      propertyName: "custom7",
      propertyKey: "custom7",
      valueKey: "custom7",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom8: {
      propertyName: "custom8",
      propertyKey: "custom8",
      valueKey: "custom8",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom9: {
      propertyName: "custom9",
      propertyKey: "custom9",
      valueKey: "custom9",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    documentCreationDate: {
      propertyName: "documentCreationDate",
      propertyKey: "documentCreationDate",
      valueKey: "document_creation_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    documentName: {
      propertyName: "documentName",
      propertyKey: "documentName",
      valueKey: "document_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    fileCreator: {
      propertyName: "fileCreator",
      propertyKey: "fileCreator",
      valueKey: "file_creator",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    fileExt: {
      propertyName: "fileExt",
      propertyKey: "fileExt",
      valueKey: "file_ext",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    fileType: {
      propertyName: "fileType",
      propertyKey: "fileType",
      valueKey: "file_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    genre: {
      propertyName: "genre",
      propertyKey: "genre",
      valueKey: "genre",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    geodata: {
      propertyName: "geodata",
      propertyKey: "geodata",
      valueKey: "geodata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    height: {
      propertyName: "height",
      propertyKey: "height",
      valueKey: "height",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageAuthor: {
      propertyName: "imageAuthor",
      propertyKey: "imageAuthor.id",
      valueKey: "image_author_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    imageAuthorEmail: {
      propertyName: "imageAuthorEmail",
      propertyKey: "imageAuthorEmail",
      valueKey: "image_author_email",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    imageAuthorName: {
      propertyName: "imageAuthorName",
      propertyKey: "imageAuthorName",
      valueKey: "image_author_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    imageLinks: {
      propertyName: "imageLinks",
      propertyKey: "imageLinks",
      valueKey: "imagelinks",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageLink"
    },
    imageSource: {
      propertyName: "imageSource",
      propertyKey: "imageSource",
      valueKey: "image_source",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    imageType: {
      propertyName: "imageType",
      propertyKey: "imageType",
      valueKey: "image_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    isProductionImage: {
      propertyName: "isProductionImage",
      propertyKey: "isProductionImage",
      valueKey: "is_production_image",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    message: {
      propertyName: "message",
      propertyKey: "message",
      valueKey: "message",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    metadata: {
      propertyName: "metadata",
      propertyKey: "metadata",
      valueKey: "metadata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    microType: {
      propertyName: "microType",
      propertyKey: "microType",
      valueKey: "micro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    mimetype: {
      propertyName: "mimetype",
      propertyKey: "mimetype",
      valueKey: "mimetype",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    minorType: {
      propertyName: "minorType",
      propertyKey: "minorType",
      valueKey: "minor_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    noArchiving: {
      propertyName: "noArchiving",
      propertyKey: "noArchiving",
      valueKey: "no_archiving",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    orientation: {
      propertyName: "orientation",
      propertyKey: "orientation",
      valueKey: "orientation",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    originalId: {
      propertyName: "originalId",
      propertyKey: "originalId",
      valueKey: "original_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    originalRemoved: {
      propertyName: "originalRemoved",
      propertyKey: "originalRemoved",
      valueKey: "original_removed",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    placeable: {
      propertyName: "placeable",
      propertyKey: "placeable",
      valueKey: "is_placeable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    privilege: {
      propertyName: "privilege",
      propertyKey: "privilege",
      valueKey: "privilege",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    refId: {
      propertyName: "refId",
      propertyKey: "refId",
      valueKey: "ref_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    reproHandler: {
      propertyName: "reproHandler",
      propertyKey: "reproHandler.id",
      valueKey: "repro_handler_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ReproHandler"
    },
    reproPath: {
      propertyName: "reproPath",
      propertyKey: "reproPath",
      valueKey: "repro_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    reproType: {
      propertyName: "reproType",
      propertyKey: "reproType",
      valueKey: "repro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    resolution: {
      propertyName: "resolution",
      propertyKey: "resolution",
      valueKey: "resolution",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    storeLocation: {
      propertyName: "storeLocation",
      propertyKey: "storeLocation.id",
      valueKey: "storelocation_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "StoreLocation"
    },
    storePath: {
      propertyName: "storePath",
      propertyKey: "storePath",
      valueKey: "storepath",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageSystemLink"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    width: {
      propertyName: "width",
      propertyKey: "width",
      valueKey: "width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    widthMm: {
      propertyName: "widthMm",
      propertyKey: "widthMm",
      valueKey: "width_mm",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  ImageContainer: {
    article: {
      propertyName: "article",
      propertyKey: "article.id",
      valueKey: "article_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Article"
    },
    articlePart: {
      propertyName: "articlePart",
      propertyKey: "articlePart.id",
      valueKey: "article_part_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticlePart"
    },
    bylines: {
      propertyName: "bylines",
      propertyKey: "bylines",
      valueKey: "byline_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Byline"
    },
    cropExists: {
      propertyName: "cropExists",
      propertyKey: "cropExists",
      valueKey: "crop_exists",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    cropProfile: {
      propertyName: "cropProfile",
      propertyKey: "cropProfile",
      valueKey: "crop_profile",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    datacrc: {
      propertyName: "datacrc",
      propertyKey: "datacrc",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageLink: {
      propertyName: "imageLink",
      propertyKey: "imageLink.id",
      valueKey: "imagelink_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ImageLink"
    },
    image_data: {
      propertyName: "image_data",
      propertyKey: "image_data",
      valueKey: "image_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "Image"
    },
    imagelink_data: {
      propertyName: "imagelink_data",
      propertyKey: "imagelink_data",
      valueKey: "imagelink_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "ImageLink"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutId: {
      propertyName: "layoutId",
      propertyKey: "layoutId",
      valueKey: "layout_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    newsMLVersion: {
      propertyName: "newsMLVersion",
      propertyKey: "newsMLVersion.id",
      valueKey: "newsml_version_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "NewsMLVersion"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    respUser: {
      propertyName: "respUser",
      propertyKey: "respUser.id",
      valueKey: "resp_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sortkey",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageContainerSystemLink"
    },
    textlength: {
      propertyName: "textlength",
      propertyKey: "textlength",
      valueKey: "textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    totalTextlength: {
      propertyName: "totalTextlength",
      propertyKey: "totalTextlength",
      valueKey: "total_textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ImageContainerSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageContainer: {
      propertyName: "imageContainer",
      propertyKey: "imageContainer.id",
      valueKey: "image_container_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ImageContainer"
    }
  },
  ImageLink: {
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    image: {
      propertyName: "image",
      propertyKey: "image.id",
      valueKey: "image_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Image"
    },
    imageContainers: {
      propertyName: "imageContainers",
      propertyKey: "imageContainers",
      valueKey: "image_container_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageContainer"
    },
    image_data: {
      propertyName: "image_data",
      propertyKey: "image_data",
      valueKey: "image_data",
      propertyType: "BASIC",
      propertyClass: "java.util.Map",
      referenceType: "Image"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  ImageSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    image: {
      propertyName: "image",
      propertyKey: "image.id",
      valueKey: "image_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Image"
    }
  },
  Job: {
    archiveId: {
      propertyName: "archiveId",
      propertyKey: "archiveId",
      valueKey: "archive_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    articles: {
      propertyName: "articles",
      propertyKey: "articles",
      valueKey: "article_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Article"
    },
    categories: {
      propertyName: "categories",
      propertyKey: "categories",
      valueKey: "categories",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    contentLinks: {
      propertyName: "contentLinks",
      propertyKey: "contentLinks",
      valueKey: "content_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ContentLink"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    deadline: {
      propertyName: "deadline",
      propertyKey: "deadline",
      valueKey: "deadline",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    deletionLocked: {
      propertyName: "deletionLocked",
      propertyKey: "deletionLocked",
      valueKey: "deletion_locked",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    geodata: {
      propertyName: "geodata",
      propertyKey: "geodata",
      valueKey: "geodata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageLinks: {
      propertyName: "imageLinks",
      propertyKey: "imageLinks",
      valueKey: "image_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ImageLink"
    },
    jobLinkReferences: {
      propertyName: "jobLinkReferences",
      propertyKey: "jobLinkReferences",
      valueKey: "job_link_reference_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "JobLinkReference"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    location: {
      propertyName: "location",
      propertyKey: "location",
      valueKey: "location",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    microType: {
      propertyName: "microType",
      propertyKey: "microType",
      valueKey: "micro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    minorType: {
      propertyName: "minorType",
      propertyKey: "minorType",
      valueKey: "minor_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    newsMLVersion: {
      propertyName: "newsMLVersion",
      propertyKey: "newsMLVersion.id",
      valueKey: "newsml_version_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "NewsMLVersion"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    photoDate: {
      propertyName: "photoDate",
      propertyKey: "photoDate",
      valueKey: "photo_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    prodDate: {
      propertyName: "prodDate",
      propertyKey: "prodDate",
      valueKey: "prod_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    prodDateEnd: {
      propertyName: "prodDateEnd",
      propertyKey: "prodDateEnd",
      valueKey: "prod_date_end",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    project: {
      propertyName: "project",
      propertyKey: "project.id",
      valueKey: "project_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Project"
    },
    removedDate: {
      propertyName: "removedDate",
      propertyKey: "removedDate",
      valueKey: "removed_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    removedFlag: {
      propertyName: "removedFlag",
      propertyKey: "removedFlag",
      valueKey: "removed_flag",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    respDepartment: {
      propertyName: "respDepartment",
      propertyKey: "respDepartment.id",
      valueKey: "resp_department",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    responsibilities: {
      propertyName: "responsibilities",
      propertyKey: "responsibilities",
      valueKey: "responsibilities",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Responsibility"
    },
    secret: {
      propertyName: "secret",
      propertyKey: "secret",
      valueKey: "secret",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "JobSystemLink"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    userdata: {
      propertyName: "userdata",
      propertyKey: "userdata",
      valueKey: "userdata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  JobLinkReference: {
    content: {
      propertyName: "content",
      propertyKey: "content",
      valueKey: "content",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    content_type: {
      propertyName: "content_type",
      propertyKey: "content_type",
      valueKey: "content_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internalRefId: {
      propertyName: "internalRefId",
      propertyKey: "internalRefId",
      valueKey: "internal_ref_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internalRefType: {
      propertyName: "internalRefType",
      propertyKey: "internalRefType",
      valueKey: "internal_ref_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    mimetype: {
      propertyName: "mimetype",
      propertyKey: "mimetype",
      valueKey: "mimetype",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    referenceType: {
      propertyName: "referenceType",
      propertyKey: "referenceType",
      valueKey: "reference_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    sortOrder: {
      propertyName: "sortOrder",
      propertyKey: "sortOrder",
      valueKey: "sort_order",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    uri: {
      propertyName: "uri",
      propertyKey: "uri",
      valueKey: "uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    uriCRC: {
      propertyName: "uriCRC",
      propertyKey: "uriCRC",
      valueKey: "uri_c_r_c",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    }
  },
  JobSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    }
  },
  LayoutDocumentTemplate: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataCrc: {
      propertyName: "dataCrc",
      propertyKey: "dataCrc",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  LibraryLayoutItem: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    itemType: {
      propertyName: "itemType",
      propertyKey: "itemType",
      valueKey: "item_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    itemXml: {
      propertyName: "itemXml",
      propertyKey: "itemXml",
      valueKey: "item_xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    }
  },
  Line: {
    approved: {
      propertyName: "approved",
      propertyKey: "approved",
      valueKey: "approved",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    approvedPage: {
      propertyName: "approvedPage",
      propertyKey: "approvedPage.id",
      valueKey: "approved_page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    approvedX1: {
      propertyName: "approvedX1",
      propertyKey: "approvedX1",
      valueKey: "approved_x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedX2: {
      propertyName: "approvedX2",
      propertyKey: "approvedX2",
      valueKey: "approved_x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedY1: {
      propertyName: "approvedY1",
      propertyKey: "approvedY1",
      valueKey: "approved_y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    approvedY2: {
      propertyName: "approvedY2",
      propertyKey: "approvedY2",
      valueKey: "approved_y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    currentPage: {
      propertyName: "currentPage",
      propertyKey: "currentPage.id",
      valueKey: "current_page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    currentX1: {
      propertyName: "currentX1",
      propertyKey: "currentX1",
      valueKey: "current_x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentX2: {
      propertyName: "currentX2",
      propertyKey: "currentX2",
      valueKey: "current_x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentY1: {
      propertyName: "currentY1",
      propertyKey: "currentY1",
      valueKey: "current_y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    currentY2: {
      propertyName: "currentY2",
      propertyKey: "currentY2",
      valueKey: "current_y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lineType: {
      propertyName: "lineType",
      propertyKey: "lineType",
      valueKey: "line_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    objectIds: {
      propertyName: "objectIds",
      propertyKey: "objectIds",
      valueKey: "object_ids",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    objectStyle: {
      propertyName: "objectStyle",
      propertyKey: "objectStyle",
      valueKey: "object_style",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    page: {
      propertyName: "page",
      propertyKey: "page.id",
      valueKey: "page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    thickness: {
      propertyName: "thickness",
      propertyKey: "thickness",
      valueKey: "thickness",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    x1: {
      propertyName: "x1",
      propertyKey: "x1",
      valueKey: "x1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    x2: {
      propertyName: "x2",
      propertyKey: "x2",
      valueKey: "x2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    y1: {
      propertyName: "y1",
      propertyKey: "y1",
      valueKey: "y1",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    y2: {
      propertyName: "y2",
      propertyKey: "y2",
      valueKey: "y2",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    }
  },
  LineProfile: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    distanceBottom: {
      propertyName: "distanceBottom",
      propertyKey: "distanceBottom",
      valueKey: "distance_bottom",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    distanceLeft: {
      propertyName: "distanceLeft",
      propertyKey: "distanceLeft",
      valueKey: "distance_left",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    distanceRight: {
      propertyName: "distanceRight",
      propertyKey: "distanceRight",
      valueKey: "distance_right",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    distanceTop: {
      propertyName: "distanceTop",
      propertyKey: "distanceTop",
      valueKey: "distance_top",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    drawBottom: {
      propertyName: "drawBottom",
      propertyKey: "drawBottom",
      valueKey: "draw_bottom",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    drawLeft: {
      propertyName: "drawLeft",
      propertyKey: "drawLeft",
      valueKey: "draw_left",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    drawRight: {
      propertyName: "drawRight",
      propertyKey: "drawRight",
      valueKey: "draw_right",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    drawTop: {
      propertyName: "drawTop",
      propertyKey: "drawTop",
      valueKey: "draw_top",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    horizontalLineClippingDistance: {
      propertyName: "horizontalLineClippingDistance",
      propertyKey: "horizontalLineClippingDistance",
      valueKey: "horizontal_line_clipping_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lineType: {
      propertyName: "lineType",
      propertyKey: "lineType",
      valueKey: "line_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    maximumHorizontalConnectionDistance: {
      propertyName: "maximumHorizontalConnectionDistance",
      propertyKey: "maximumHorizontalConnectionDistance",
      valueKey: "maximum_horizontal_connection_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    maximumHorizontalMergeVerticalDistance: {
      propertyName: "maximumHorizontalMergeVerticalDistance",
      propertyKey: "maximumHorizontalMergeVerticalDistance",
      valueKey: "maximum_horizontal_merge_vertical_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    maximumVerticalConnectionDistance: {
      propertyName: "maximumVerticalConnectionDistance",
      propertyKey: "maximumVerticalConnectionDistance",
      valueKey: "maximum_vertical_connection_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    maximumVerticalMergeHorizontalDistance: {
      propertyName: "maximumVerticalMergeHorizontalDistance",
      propertyKey: "maximumVerticalMergeHorizontalDistance",
      valueKey: "maximum_vertical_merge_horizontal_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    maximumVerticalMergeVerticalDistance: {
      propertyName: "maximumVerticalMergeVerticalDistance",
      propertyKey: "maximumVerticalMergeVerticalDistance",
      valueKey: "maximum_vertical_merge_vertical_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    mergeVertical: {
      propertyName: "mergeVertical",
      propertyKey: "mergeVertical",
      valueKey: "merge_vertical",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    minimumHorizontalDistance: {
      propertyName: "minimumHorizontalDistance",
      propertyKey: "minimumHorizontalDistance",
      valueKey: "minimum_horizontal_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    minimumLineLength: {
      propertyName: "minimumLineLength",
      propertyKey: "minimumLineLength",
      valueKey: "minimum_line_length",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    minimumVerticalDistanceAbove: {
      propertyName: "minimumVerticalDistanceAbove",
      propertyKey: "minimumVerticalDistanceAbove",
      valueKey: "minimum_vertical_distance_above",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    minimumVerticalDistanceBelow: {
      propertyName: "minimumVerticalDistanceBelow",
      propertyKey: "minimumVerticalDistanceBelow",
      valueKey: "minimum_vertical_distance_below",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    objectStyle: {
      propertyName: "objectStyle",
      propertyKey: "objectStyle",
      valueKey: "object_style",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    shorteningHorizontalDistance: {
      propertyName: "shorteningHorizontalDistance",
      propertyKey: "shorteningHorizontalDistance",
      valueKey: "shortening_horizontal_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    shorteningVerticalDistance: {
      propertyName: "shorteningVerticalDistance",
      propertyKey: "shorteningVerticalDistance",
      valueKey: "shortening_vertical_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    sortKey: {
      propertyName: "sortKey",
      propertyKey: "sortKey",
      valueKey: "sort_key",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    verticalLineClippingDistance: {
      propertyName: "verticalLineClippingDistance",
      propertyKey: "verticalLineClippingDistance",
      valueKey: "vertical_line_clipping_distance",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    }
  },
  LinguisticProposal: {
    corrected: {
      propertyName: "corrected",
      propertyKey: "corrected",
      valueKey: "corrected",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    language: {
      propertyName: "language",
      propertyKey: "language",
      valueKey: "language",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    linguisticType: {
      propertyName: "linguisticType",
      propertyKey: "linguisticType",
      valueKey: "linguistic_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ModuleUsage: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    moduleId: {
      propertyName: "moduleId",
      propertyKey: "moduleId",
      valueKey: "module_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    session: {
      propertyName: "session",
      propertyKey: "session.id",
      valueKey: "session_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserSessionInfo"
    }
  },
  NILayoutModel: {
    crc: {
      propertyName: "crc",
      propertyKey: "crc",
      valueKey: "crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    data: {
      propertyName: "data",
      propertyKey: "data",
      valueKey: "data",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    majorType: {
      propertyName: "majorType",
      propertyKey: "majorType",
      valueKey: "entity_major_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    microType: {
      propertyName: "microType",
      propertyKey: "microType",
      valueKey: "entity_micro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    minorType: {
      propertyName: "minorType",
      propertyKey: "minorType",
      valueKey: "entity_minor_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organizationId: {
      propertyName: "organizationId",
      propertyKey: "organizationId",
      valueKey: "organization_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updated: {
      propertyName: "updated",
      propertyKey: "updated",
      valueKey: "updated",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    }
  },
  NewsMLVersion: {
    canceled: {
      propertyName: "canceled",
      propertyKey: "canceled",
      valueKey: "canceled",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    provider: {
      propertyName: "provider",
      propertyKey: "provider.id",
      valueKey: "provider_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    version: {
      propertyName: "version",
      propertyKey: "version",
      valueKey: "version",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    }
  },
  Organization: {
    defaultPrivilegeGroup: {
      propertyName: "defaultPrivilegeGroup",
      propertyKey: "defaultPrivilegeGroup.id",
      valueKey: "default_privilege_group_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PrivilegeGroup"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    macBylinePath: {
      propertyName: "macBylinePath",
      propertyKey: "macBylinePath",
      valueKey: "mac_byline_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organizationalGroup: {
      propertyName: "organizationalGroup",
      propertyKey: "organizationalGroup.id",
      valueKey: "organizational_group_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "OrganizationalGroup"
    },
    pcBylinePath: {
      propertyName: "pcBylinePath",
      propertyKey: "pcBylinePath",
      valueKey: "pc_byline_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  OrganizationSetting: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    key: {
      propertyName: "key",
      propertyKey: "key",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    value: {
      propertyName: "value",
      propertyKey: "value",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  OrganizationalGroup: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Page: {
    approvedAds: {
      propertyName: "approvedAds",
      propertyKey: "approvedAds",
      valueKey: "approved_ad_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Ad"
    },
    archiveId: {
      propertyName: "archiveId",
      propertyKey: "archiveId",
      valueKey: "archive_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    arkitexData: {
      propertyName: "arkitexData",
      propertyKey: "arkitexData",
      valueKey: "arkitex_data",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    articles: {
      propertyName: "articles",
      propertyKey: "articles",
      valueKey: "article_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Article"
    },
    bleed: {
      propertyName: "bleed",
      propertyKey: "bleed",
      valueKey: "bleed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    bookedSection: {
      propertyName: "bookedSection",
      propertyKey: "bookedSection.id",
      valueKey: "booked_section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    bookedTemplate: {
      propertyName: "bookedTemplate",
      propertyKey: "bookedTemplate",
      valueKey: "booked_template",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    bottomMargin: {
      propertyName: "bottomMargin",
      propertyKey: "bottomMargin",
      valueKey: "bottom_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    children: {
      propertyName: "children",
      propertyKey: "children",
      valueKey: "children_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Page"
    },
    color: {
      propertyName: "color",
      propertyKey: "color",
      valueKey: "color",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    columnCount: {
      propertyName: "columnCount",
      propertyKey: "columnCount",
      valueKey: "column_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    columnWidth: {
      propertyName: "columnWidth",
      propertyKey: "columnWidth",
      valueKey: "column_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    controlFile: {
      propertyName: "controlFile",
      propertyKey: "controlFile",
      valueKey: "control_file",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    custom1: {
      propertyName: "custom1",
      propertyKey: "custom1",
      valueKey: "custom1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom2: {
      propertyName: "custom2",
      propertyKey: "custom2",
      valueKey: "custom2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom3: {
      propertyName: "custom3",
      propertyKey: "custom3",
      valueKey: "custom3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom4: {
      propertyName: "custom4",
      propertyKey: "custom4",
      valueKey: "custom4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom5: {
      propertyName: "custom5",
      propertyKey: "custom5",
      valueKey: "custom5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    deadline: {
      propertyName: "deadline",
      propertyKey: "deadline",
      valueKey: "deadline",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    descriptiveName: {
      propertyName: "descriptiveName",
      propertyKey: "descriptiveName",
      valueKey: "descriptive_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    dirty: {
      propertyName: "dirty",
      propertyKey: "dirty",
      valueKey: "dirty",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    documentMacro: {
      propertyName: "documentMacro",
      propertyKey: "documentMacro",
      valueKey: "document_macro",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    documentName: {
      propertyName: "documentName",
      propertyKey: "documentName",
      valueKey: "document_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    documentType: {
      propertyName: "documentType",
      propertyKey: "documentType",
      valueKey: "document_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    edType: {
      propertyName: "edType",
      propertyKey: "edType",
      valueKey: "ed_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    editable: {
      propertyName: "editable",
      propertyKey: "editable",
      valueKey: "editable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    edition: {
      propertyName: "edition",
      propertyKey: "edition",
      valueKey: "edition",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    entityLock: {
      propertyName: "entityLock",
      propertyKey: "entityLock.id",
      valueKey: "entitylock_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityLock"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    fileExists: {
      propertyName: "fileExists",
      propertyKey: "fileExists",
      valueKey: "file_exists",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    firstPagin: {
      propertyName: "firstPagin",
      propertyKey: "firstPagin",
      valueKey: "first_pagin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    guid: {
      propertyName: "guid",
      propertyKey: "guid",
      valueKey: "guid",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    gutterWidth: {
      propertyName: "gutterWidth",
      propertyKey: "gutterWidth",
      valueKey: "gutter_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    innerMargin: {
      propertyName: "innerMargin",
      propertyKey: "innerMargin",
      valueKey: "inner_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    joinWithPrev: {
      propertyName: "joinWithPrev",
      propertyKey: "joinWithPrev",
      valueKey: "join_with_prev",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lines: {
      propertyName: "lines",
      propertyKey: "lines",
      valueKey: "line_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Line"
    },
    master: {
      propertyName: "master",
      propertyKey: "master.id",
      valueKey: "master_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    masterName: {
      propertyName: "masterName",
      propertyKey: "masterName",
      valueKey: "master_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    masterPage: {
      propertyName: "masterPage",
      propertyKey: "masterPage",
      valueKey: "is_master_page",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    masterSpread: {
      propertyName: "masterSpread",
      propertyKey: "masterSpread",
      valueKey: "master_spread",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    message: {
      propertyName: "message",
      propertyKey: "message",
      valueKey: "message",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    multiPageCount: {
      propertyName: "multiPageCount",
      propertyKey: "multiPageCount",
      valueKey: "multi_page_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    multiPagePos: {
      propertyName: "multiPagePos",
      propertyKey: "multiPagePos",
      valueKey: "multi_page_pos",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    nameMacro: {
      propertyName: "nameMacro",
      propertyKey: "nameMacro",
      valueKey: "name_macro",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    outerMargin: {
      propertyName: "outerMargin",
      propertyKey: "outerMargin",
      valueKey: "outer_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    pageLayoutTemplates: {
      propertyName: "pageLayoutTemplates",
      propertyKey: "pageLayoutTemplates",
      valueKey: "page_layout_template_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "PageLayoutTemplate"
    },
    pageNumber: {
      propertyName: "pageNumber",
      propertyKey: "pageNumber",
      valueKey: "page_number",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    part: {
      propertyName: "part",
      propertyKey: "part",
      valueKey: "part",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    physicalHeight: {
      propertyName: "physicalHeight",
      propertyKey: "physicalHeight",
      valueKey: "physical_height",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    physicalWidth: {
      propertyName: "physicalWidth",
      propertyKey: "physicalWidth",
      valueKey: "physical_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    plate: {
      propertyName: "plate",
      propertyKey: "plate",
      valueKey: "plate",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    preproductionType: {
      propertyName: "preproductionType",
      propertyKey: "preproductionType",
      valueKey: "preproduction_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    previewCRC: {
      propertyName: "previewCRC",
      propertyKey: "previewCRC",
      valueKey: "preview_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    previewCreator: {
      propertyName: "previewCreator",
      propertyKey: "previewCreator",
      valueKey: "preview_creator",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    printDateBw: {
      propertyName: "printDateBw",
      propertyKey: "printDateBw",
      valueKey: "print_date_bw",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    printDateCmyk: {
      propertyName: "printDateCmyk",
      propertyKey: "printDateCmyk",
      valueKey: "print_date_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    printMacroBw: {
      propertyName: "printMacroBw",
      propertyKey: "printMacroBw",
      valueKey: "print_macro_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    printMacroCmyk: {
      propertyName: "printMacroCmyk",
      propertyKey: "printMacroCmyk",
      valueKey: "print_macro_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    printVersionBw: {
      propertyName: "printVersionBw",
      propertyKey: "printVersionBw",
      valueKey: "print_version_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    printVersionCmyk: {
      propertyName: "printVersionCmyk",
      propertyKey: "printVersionCmyk",
      valueKey: "print_version_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    printableBw: {
      propertyName: "printableBw",
      propertyKey: "printableBw",
      valueKey: "printable_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    printableCmyk: {
      propertyName: "printableCmyk",
      propertyKey: "printableCmyk",
      valueKey: "printable_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    publicationDate: {
      propertyName: "publicationDate",
      propertyKey: "publicationDate.id",
      valueKey: "publication_date_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PublicationDate"
    },
    reference: {
      propertyName: "reference",
      propertyKey: "reference",
      valueKey: "reference",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    respUser: {
      propertyName: "respUser",
      propertyKey: "respUser.id",
      valueKey: "resp_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    restored: {
      propertyName: "restored",
      propertyKey: "restored",
      valueKey: "restored_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    sectionOverride: {
      propertyName: "sectionOverride",
      propertyKey: "sectionOverride",
      valueKey: "section_override",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    sequence: {
      propertyName: "sequence",
      propertyKey: "sequence",
      valueKey: "sequence",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "PageSystemLink"
    },
    tags: {
      propertyName: "tags",
      propertyKey: "tags",
      valueKey: "tags",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    template: {
      propertyName: "template",
      propertyKey: "template",
      valueKey: "template",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    templateOverride: {
      propertyName: "templateOverride",
      propertyKey: "templateOverride",
      valueKey: "template_override",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    thumbCRC: {
      propertyName: "thumbCRC",
      propertyKey: "thumbCRC",
      valueKey: "thumb_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    topMargin: {
      propertyName: "topMargin",
      propertyKey: "topMargin",
      valueKey: "top_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    uniqueId: {
      propertyName: "uniqueId",
      propertyKey: "uniqueId",
      valueKey: "unique_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    unlockedToken: {
      propertyName: "unlockedToken",
      propertyKey: "unlockedToken",
      valueKey: "unlocked_token",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    version: {
      propertyName: "version",
      propertyKey: "version",
      valueKey: "version",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  PageFormat: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    settings: {
      propertyName: "settings",
      propertyKey: "settings",
      valueKey: "settings",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  PageLayoutTemplate: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    layoutXml: {
      propertyName: "layoutXml",
      propertyKey: "layoutXml",
      valueKey: "layout_xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    page: {
      propertyName: "page",
      propertyKey: "page.id",
      valueKey: "page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    },
    templateType: {
      propertyName: "templateType",
      propertyKey: "templateType",
      valueKey: "template_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  PageSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    page: {
      propertyName: "page",
      propertyKey: "page.id",
      valueKey: "page_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Page"
    }
  },
  PageTemplate: {
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    }
  },
  Placement: {
    adClass: {
      propertyName: "adClass",
      propertyKey: "adClass.id",
      valueKey: "ad_class_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "AdClass"
    },
    color: {
      propertyName: "color",
      propertyKey: "color",
      valueKey: "color",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parent: {
      propertyName: "parent",
      propertyKey: "parent.id",
      valueKey: "parent_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Placement"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    }
  },
  PrintChannelLink: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    defaultPrintChannel: {
      propertyName: "defaultPrintChannel",
      propertyKey: "defaultPrintChannel",
      valueKey: "default_print_channel",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    printChannel: {
      propertyName: "printChannel",
      propertyKey: "printChannel.id",
      valueKey: "print_channel_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ServerPrintChannel"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    }
  },
  PrintFlow: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    serviceWatches: {
      propertyName: "serviceWatches",
      propertyKey: "serviceWatches",
      valueKey: "servicewatches",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ServiceWatch"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  Privilege: {
    action: {
      propertyName: "action",
      propertyKey: "action",
      valueKey: "action",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    entityType: {
      propertyName: "entityType",
      propertyKey: "entityType",
      valueKey: "entity_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    privilegeGroup: {
      propertyName: "privilegeGroup",
      propertyKey: "privilegeGroup.id",
      valueKey: "privilege_group_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PrivilegeGroup"
    },
    scope: {
      propertyName: "scope",
      propertyKey: "scope",
      valueKey: "scope",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  PrivilegeGroup: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    privileges: {
      propertyName: "privileges",
      propertyKey: "privileges",
      valueKey: "privilege_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Privilege"
    }
  },
  Product: {
    articleTypes: {
      propertyName: "articleTypes",
      propertyKey: "articleTypes",
      valueKey: "article_type_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticleType"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    iconLarge: {
      propertyName: "iconLarge",
      propertyKey: "iconLarge",
      valueKey: "icon_large",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    iconSmall: {
      propertyName: "iconSmall",
      propertyKey: "iconSmall",
      valueKey: "icon_small",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    locale: {
      propertyName: "locale",
      propertyKey: "locale",
      valueKey: "locale",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mediaId: {
      propertyName: "mediaId",
      propertyKey: "mediaId",
      valueKey: "media_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    subProducts: {
      propertyName: "subProducts",
      propertyKey: "subProducts",
      valueKey: "sub_product_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "SubProduct"
    },
    usersAllowed: {
      propertyName: "usersAllowed",
      propertyKey: "usersAllowed",
      valueKey: "users_allowed_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "User"
    },
    usersDefault: {
      propertyName: "usersDefault",
      propertyKey: "usersDefault",
      valueKey: "users_default_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "User"
    }
  },
  ProductSetting: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    key: {
      propertyName: "key",
      propertyKey: "key",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    value: {
      propertyName: "value",
      propertyKey: "value",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Profile: {
    defaultProfile: {
      propertyName: "defaultProfile",
      propertyKey: "defaultProfile",
      valueKey: "default_profile",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    xml: {
      propertyName: "xml",
      propertyKey: "xml",
      valueKey: "xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Project: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    jobs: {
      propertyName: "jobs",
      propertyKey: "jobs",
      valueKey: "jobs",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Job"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ProjectSystemLink"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ProjectSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    project: {
      propertyName: "project",
      propertyKey: "project.id",
      valueKey: "project_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Project"
    }
  },
  PublicationDate: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    issueNumber: {
      propertyName: "issueNumber",
      propertyKey: "issueNumber",
      valueKey: "issuenumber",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    pubDate: {
      propertyName: "pubDate",
      propertyKey: "pubDate",
      valueKey: "pub_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    }
  },
  PublicationSetting: {
    articleType: {
      propertyName: "articleType",
      propertyKey: "articleType.id",
      valueKey: "article_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleType"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    defaultDate: {
      propertyName: "defaultDate",
      propertyKey: "defaultDate",
      valueKey: "default_date",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.PublicationSetting.DefaultDate"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    section: {
      propertyName: "section",
      propertyKey: "section.id",
      valueKey: "section_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  QueryTemplate: {
    allowWhitespaceDrop: {
      propertyName: "allowWhitespaceDrop",
      propertyKey: "allowWhitespaceDrop",
      valueKey: "allow_whitespace_drop",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    departments: {
      propertyName: "departments",
      propertyKey: "departments",
      valueKey: "departments",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Department"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    queryTemplateShortcuts: {
      propertyName: "queryTemplateShortcuts",
      propertyKey: "queryTemplateShortcuts",
      valueKey: "query_template_shortcut_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "QueryTemplateShortcut"
    },
    roles: {
      propertyName: "roles",
      propertyKey: "roles",
      valueKey: "roles",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "UserRole"
    },
    xml: {
      propertyName: "xml",
      propertyKey: "xml",
      valueKey: "xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  QueryTemplateShortcut: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    queryTemplate: {
      propertyName: "queryTemplate",
      propertyKey: "queryTemplate.id",
      valueKey: "query_template_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "QueryTemplate"
    },
    queryTemplateControls: {
      propertyName: "queryTemplateControls",
      propertyKey: "queryTemplateControls",
      valueKey: "query_template_controls",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ReproHandler: {
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organizations: {
      propertyName: "organizations",
      propertyKey: "organizations",
      valueKey: "organizations",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Organization"
    },
    xmlSettings: {
      propertyName: "xmlSettings",
      propertyKey: "xmlSettings",
      valueKey: "xml_settings",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Resource: {
    crc: {
      propertyName: "crc",
      propertyKey: "crc",
      valueKey: "crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    extension: {
      propertyName: "extension",
      propertyKey: "extension",
      valueKey: "extension",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    resourceType: {
      propertyName: "resourceType",
      propertyKey: "resourceType",
      valueKey: "resource_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    variation: {
      propertyName: "variation",
      propertyKey: "variation",
      valueKey: "variation",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  ResourceSchedule: {
    approved: {
      propertyName: "approved",
      propertyKey: "approved",
      valueKey: "approved",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    days: {
      propertyName: "days",
      propertyKey: "days",
      valueKey: "days",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    }
  },
  ResourceScheduleEvent: {
    calendarEventType: {
      propertyName: "calendarEventType",
      propertyKey: "calendarEventType.id",
      valueKey: "calendar_event_type_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "CalendarEventType"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    endDate: {
      propertyName: "endDate",
      propertyKey: "endDate",
      valueKey: "end_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    resourceSchedule: {
      propertyName: "resourceSchedule",
      propertyKey: "resourceSchedule.id",
      valueKey: "resource_schedule_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ResourceSchedule"
    },
    resourceStatus: {
      propertyName: "resourceStatus",
      propertyKey: "resourceStatus.id",
      valueKey: "resource_status_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ResourceStatus"
    },
    startDate: {
      propertyName: "startDate",
      propertyKey: "startDate",
      valueKey: "start_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    summary: {
      propertyName: "summary",
      propertyKey: "summary",
      valueKey: "summary",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    totalTime: {
      propertyName: "totalTime",
      propertyKey: "totalTime",
      valueKey: "total_time",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    userRole: {
      propertyName: "userRole",
      propertyKey: "userRole.id",
      valueKey: "user_role_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserRole"
    }
  },
  ResourceStatus: {
    color: {
      propertyName: "color",
      propertyKey: "color",
      valueKey: "color",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    isAbsence: {
      propertyName: "isAbsence",
      propertyKey: "isAbsence",
      valueKey: "is_absence",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Responsibility: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    job: {
      propertyName: "job",
      propertyKey: "job.id",
      valueKey: "job_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Job"
    },
    manual: {
      propertyName: "manual",
      propertyKey: "manual",
      valueKey: "manual",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    userRole: {
      propertyName: "userRole",
      propertyKey: "userRole.id",
      valueKey: "user_role_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserRole"
    }
  },
  Section: {
    defaultArticleConfig: {
      propertyName: "defaultArticleConfig",
      propertyKey: "defaultArticleConfig.id",
      valueKey: "default_article_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ArticleConfig"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    invisible: {
      propertyName: "invisible",
      propertyKey: "invisible",
      valueKey: "invisible",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    parent: {
      propertyName: "parent",
      propertyKey: "parent.id",
      valueKey: "parent_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Section"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    useCaptionProposed: {
      propertyName: "useCaptionProposed",
      propertyKey: "useCaptionProposed",
      valueKey: "use_caption_proposed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  ServerPrintChannel: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    dataCrc: {
      propertyName: "dataCrc",
      propertyKey: "dataCrc",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lockColorAfterPrint: {
      propertyName: "lockColorAfterPrint",
      propertyKey: "lockColorAfterPrint",
      valueKey: "lock_color_after_print",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    lockEditableAfterPrint: {
      propertyName: "lockEditableAfterPrint",
      propertyKey: "lockEditableAfterPrint",
      valueKey: "lock_editable_after_print",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    lockGrayscaleAfterPrint: {
      propertyName: "lockGrayscaleAfterPrint",
      propertyKey: "lockGrayscaleAfterPrint",
      valueKey: "lock_grayscale_after_print",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    pageStatusFailure: {
      propertyName: "pageStatusFailure",
      propertyKey: "pageStatusFailure.id",
      valueKey: "page_status_failure",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    pageStatusSuccess: {
      propertyName: "pageStatusSuccess",
      propertyKey: "pageStatusSuccess.id",
      valueKey: "page_status_success",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    servertype: {
      propertyName: "servertype",
      propertyKey: "servertype",
      valueKey: "servertype",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    writebackAfterPrint: {
      propertyName: "writebackAfterPrint",
      propertyKey: "writebackAfterPrint",
      valueKey: "writeback_after_print",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  ServiceProvider: {
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    serviceProviderType: {
      propertyName: "serviceProviderType",
      propertyKey: "serviceProviderType",
      valueKey: "serviceprovidertype",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.service.common.ServiceProviderType"
    },
    state: {
      propertyName: "state",
      propertyKey: "state",
      valueKey: "state",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "userid",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ServiceWatch: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lastActivity: {
      propertyName: "lastActivity",
      propertyKey: "lastActivity",
      valueKey: "last_activity",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    limited: {
      propertyName: "limited",
      propertyKey: "limited",
      valueKey: "limited",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    options: {
      propertyName: "options",
      propertyKey: "options",
      valueKey: "options",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    serviceWatchLinks: {
      propertyName: "serviceWatchLinks",
      propertyKey: "serviceWatchLinks",
      valueKey: "links",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ServiceWatchLink"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    statusMessage: {
      propertyName: "statusMessage",
      propertyKey: "statusMessage",
      valueKey: "status_message",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ServiceWatchLink: {
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    dataCRC: {
      propertyName: "dataCRC",
      propertyKey: "dataCRC",
      valueKey: "data_crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    serviceProvider: {
      propertyName: "serviceProvider",
      propertyKey: "serviceProvider.id",
      valueKey: "serviceprovider_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ServiceProvider"
    },
    serviceWatch: {
      propertyName: "serviceWatch",
      propertyKey: "serviceWatch.id",
      valueKey: "servicewatch_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ServiceWatch"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    statusMessage: {
      propertyName: "statusMessage",
      propertyKey: "statusMessage",
      valueKey: "status_message",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  SpecialChar: {
    charKey: {
      propertyName: "charKey",
      propertyKey: "charKey",
      valueKey: "char_key",
      propertyType: "BASIC",
      propertyClass: "java.lang.Character"
    },
    icon: {
      propertyName: "icon",
      propertyKey: "icon",
      valueKey: "icon",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    order: {
      propertyName: "order",
      propertyKey: "order",
      valueKey: "order",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    substitute: {
      propertyName: "substitute",
      propertyKey: "substitute",
      valueKey: "substitute",
      propertyType: "BASIC",
      propertyClass: "java.lang.Character"
    }
  },
  Status: {
    color: {
      propertyName: "color",
      propertyKey: "color",
      valueKey: "color",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    locked: {
      propertyName: "locked",
      propertyKey: "locked",
      valueKey: "locked",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sortkey: {
      propertyName: "sortkey",
      propertyKey: "sortkey",
      valueKey: "sort_key",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  StoreLocation: {
    archive: {
      propertyName: "archive",
      propertyKey: "archive",
      valueKey: "archive",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    autoDeleteExtImage: {
      propertyName: "autoDeleteExtImage",
      propertyKey: "autoDeleteExtImage",
      valueKey: "auto_delete_ext_image",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    backgroundMaterial: {
      propertyName: "backgroundMaterial",
      propertyKey: "backgroundMaterial",
      valueKey: "background_material",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    blockedImportExtensions: {
      propertyName: "blockedImportExtensions",
      propertyKey: "blockedImportExtensions",
      valueKey: "blocked_import_extensions",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createLinkReference: {
      propertyName: "createLinkReference",
      propertyKey: "createLinkReference",
      valueKey: "create_link_reference",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    entityFlag: {
      propertyName: "entityFlag",
      propertyKey: "entityFlag.id",
      valueKey: "entity_flag_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "EntityFlag"
    },
    exportPath: {
      propertyName: "exportPath",
      propertyKey: "exportPath",
      valueKey: "export_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    exportStoreLocation: {
      propertyName: "exportStoreLocation",
      propertyKey: "exportStoreLocation.id",
      valueKey: "export_storelocation_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "StoreLocation"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    fileDistribution: {
      propertyName: "fileDistribution",
      propertyKey: "fileDistribution",
      valueKey: "file_distribution",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    fileNameLength: {
      propertyName: "fileNameLength",
      propertyKey: "fileNameLength",
      valueKey: "file_name_length",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    filePathMac: {
      propertyName: "filePathMac",
      propertyKey: "filePathMac",
      valueKey: "filepath_mac",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    filePathWin: {
      propertyName: "filePathWin",
      propertyKey: "filePathWin",
      valueKey: "filepath_win",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageType: {
      propertyName: "imageType",
      propertyKey: "imageType",
      valueKey: "image_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    importExtensions: {
      propertyName: "importExtensions",
      propertyKey: "importExtensions",
      valueKey: "import_extensions",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    login: {
      propertyName: "login",
      propertyKey: "login",
      valueKey: "login",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    loginType: {
      propertyName: "loginType",
      propertyKey: "loginType",
      valueKey: "login_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    mountable: {
      propertyName: "mountable",
      propertyKey: "mountable",
      valueKey: "mountable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    openPath: {
      propertyName: "openPath",
      propertyKey: "openPath",
      valueKey: "open_path",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    password: {
      propertyName: "password",
      propertyKey: "password",
      valueKey: "password",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    preProcessing: {
      propertyName: "preProcessing",
      propertyKey: "preProcessing",
      valueKey: "pre_processing",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    preventManualEditing: {
      propertyName: "preventManualEditing",
      propertyKey: "preventManualEditing",
      valueKey: "prevent_editing",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    showImportPreview: {
      propertyName: "showImportPreview",
      propertyKey: "showImportPreview",
      valueKey: "show_import_preview",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    showImportWindow: {
      propertyName: "showImportWindow",
      propertyKey: "showImportWindow",
      valueKey: "show_import_window",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    showPreProcess: {
      propertyName: "showPreProcess",
      propertyKey: "showPreProcess",
      valueKey: "show_pre_process",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    storageKind: {
      propertyName: "storageKind",
      propertyKey: "storageKind",
      valueKey: "storage_kind",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    storeType: {
      propertyName: "storeType",
      propertyKey: "storeType",
      valueKey: "storetype",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updateArchiveInfo: {
      propertyName: "updateArchiveInfo",
      propertyKey: "updateArchiveInfo",
      valueKey: "update_archive_info",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    urlPath: {
      propertyName: "urlPath",
      propertyKey: "urlPath",
      valueKey: "urlpath",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  SubProduct: {
    allowCreateEditionPages: {
      propertyName: "allowCreateEditionPages",
      propertyKey: "allowCreateEditionPages",
      valueKey: "allow_create_edition_pages",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    allowCreateVersionPages: {
      propertyName: "allowCreateVersionPages",
      propertyKey: "allowCreateVersionPages",
      valueKey: "allow_create_version_pages",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    autoDocname: {
      propertyName: "autoDocname",
      propertyKey: "autoDocname",
      valueKey: "auto_docname",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    autoPagename: {
      propertyName: "autoPagename",
      propertyKey: "autoPagename",
      valueKey: "auto_pagename",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    bleed: {
      propertyName: "bleed",
      propertyKey: "bleed",
      valueKey: "bleed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    bottomMargin: {
      propertyName: "bottomMargin",
      propertyKey: "bottomMargin",
      valueKey: "bottom_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    columnCount: {
      propertyName: "columnCount",
      propertyKey: "columnCount",
      valueKey: "column_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    createNewForReleasedPage: {
      propertyName: "createNewForReleasedPage",
      propertyKey: "createNewForReleasedPage",
      valueKey: "create_new_for_released_page",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    createPagesOnJoin: {
      propertyName: "createPagesOnJoin",
      propertyKey: "createPagesOnJoin",
      valueKey: "create_pages_on_join",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    defaultDocExtension: {
      propertyName: "defaultDocExtension",
      propertyKey: "defaultDocExtension",
      valueKey: "default_doc_extension",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    defaultEditionType: {
      propertyName: "defaultEditionType",
      propertyKey: "defaultEditionType",
      valueKey: "default_edition_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    defaultPageArticleBehavior: {
      propertyName: "defaultPageArticleBehavior",
      propertyKey: "defaultPageArticleBehavior",
      valueKey: "default_page_article_behavior",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    editable: {
      propertyName: "editable",
      propertyKey: "editable",
      valueKey: "editable",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    gutterWidth: {
      propertyName: "gutterWidth",
      propertyKey: "gutterWidth",
      valueKey: "gutter_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    innerMargin: {
      propertyName: "innerMargin",
      propertyKey: "innerMargin",
      valueKey: "inner_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    localPrintEnabled: {
      propertyName: "localPrintEnabled",
      propertyKey: "localPrintEnabled",
      valueKey: "local_print_enabled",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    outerMargin: {
      propertyName: "outerMargin",
      propertyKey: "outerMargin",
      valueKey: "outer_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    pageFlags: {
      propertyName: "pageFlags",
      propertyKey: "pageFlags",
      valueKey: "page_flag_ids",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    physicalHeight: {
      propertyName: "physicalHeight",
      propertyKey: "physicalHeight",
      valueKey: "physical_height",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    physicalWidth: {
      propertyName: "physicalWidth",
      propertyKey: "physicalWidth",
      valueKey: "physical_width",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    printChannelLink: {
      propertyName: "printChannelLink",
      propertyKey: "printChannelLink",
      valueKey: "printchannel_links",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "PrintChannelLink"
    },
    printMacroBw: {
      propertyName: "printMacroBw",
      propertyKey: "printMacroBw",
      valueKey: "print_macro_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    printMacroCmyk: {
      propertyName: "printMacroCmyk",
      propertyKey: "printMacroCmyk",
      valueKey: "print_macro_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    printableColor: {
      propertyName: "printableColor",
      propertyKey: "printableColor",
      valueKey: "printable_color",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    printableGray: {
      propertyName: "printableGray",
      propertyKey: "printableGray",
      valueKey: "printable_gray",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    rackCard: {
      propertyName: "rackCard",
      propertyKey: "rackCard",
      valueKey: "is_rack_card",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    rackCardSubProduct: {
      propertyName: "rackCardSubProduct",
      propertyKey: "rackCardSubProduct.id",
      valueKey: "rack_card_sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    renameDocumentAllowed: {
      propertyName: "renameDocumentAllowed",
      propertyKey: "renameDocumentAllowed",
      valueKey: "rename_document_allowed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    settings: {
      propertyName: "settings",
      propertyKey: "settings",
      valueKey: "settings",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    startVersionBw: {
      propertyName: "startVersionBw",
      propertyKey: "startVersionBw",
      valueKey: "start_version_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    startVersionCmyk: {
      propertyName: "startVersionCmyk",
      propertyKey: "startVersionCmyk",
      valueKey: "start_version_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    topMargin: {
      propertyName: "topMargin",
      propertyKey: "topMargin",
      valueKey: "top_margin",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    versionIncrementBw: {
      propertyName: "versionIncrementBw",
      propertyKey: "versionIncrementBw",
      valueKey: "version_increment_bw",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    versionIncrementCmyk: {
      propertyName: "versionIncrementCmyk",
      propertyKey: "versionIncrementCmyk",
      valueKey: "version_increment_cmyk",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    warnWhenOpenPageCopy: {
      propertyName: "warnWhenOpenPageCopy",
      propertyKey: "warnWhenOpenPageCopy",
      valueKey: "warn_when_open_page_copy",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    }
  },
  SystemSetting: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    key: {
      propertyName: "key",
      propertyKey: "key",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    value: {
      propertyName: "value",
      propertyKey: "value",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Tag: {
    blocked: {
      propertyName: "blocked",
      propertyKey: "blocked",
      valueKey: "blocked",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    permanent: {
      propertyName: "permanent",
      propertyKey: "permanent",
      valueKey: "permanent",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    tagType: {
      propertyName: "tagType",
      propertyKey: "tagType",
      valueKey: "tag_type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.TagType"
    },
    text: {
      propertyName: "text",
      propertyKey: "text",
      valueKey: "text",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  Telegram: {
    byteSize: {
      propertyName: "byteSize",
      propertyKey: "byteSize",
      valueKey: "byte_size",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createdDate: {
      propertyName: "createdDate",
      propertyKey: "createdDate",
      valueKey: "created_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    createdUser: {
      propertyName: "createdUser",
      propertyKey: "createdUser.id",
      valueKey: "created_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    custom1: {
      propertyName: "custom1",
      propertyKey: "custom1",
      valueKey: "custom_1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom10: {
      propertyName: "custom10",
      propertyKey: "custom10",
      valueKey: "custom_10",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom2: {
      propertyName: "custom2",
      propertyKey: "custom2",
      valueKey: "custom_2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom3: {
      propertyName: "custom3",
      propertyKey: "custom3",
      valueKey: "custom_3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom4: {
      propertyName: "custom4",
      propertyKey: "custom4",
      valueKey: "custom_4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom5: {
      propertyName: "custom5",
      propertyKey: "custom5",
      valueKey: "custom_5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom6: {
      propertyName: "custom6",
      propertyKey: "custom6",
      valueKey: "custom_6",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom7: {
      propertyName: "custom7",
      propertyKey: "custom7",
      valueKey: "custom_7",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom8: {
      propertyName: "custom8",
      propertyKey: "custom8",
      valueKey: "custom_8",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom9: {
      propertyName: "custom9",
      propertyKey: "custom9",
      valueKey: "custom_9",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    destination: {
      propertyName: "destination",
      propertyKey: "destination",
      valueKey: "destination",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    edstat: {
      propertyName: "edstat",
      propertyKey: "edstat",
      valueKey: "edstat",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    flags: {
      propertyName: "flags",
      propertyKey: "flags",
      valueKey: "flags",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "EntityFlag"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    label: {
      propertyName: "label",
      propertyKey: "label",
      valueKey: "label",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    links: {
      propertyName: "links",
      propertyKey: "links",
      valueKey: "links",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    message: {
      propertyName: "message",
      propertyKey: "message",
      valueKey: "message",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    meta: {
      propertyName: "meta",
      propertyKey: "meta",
      valueKey: "metadata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    microType: {
      propertyName: "microType",
      propertyKey: "microType",
      valueKey: "micro_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    minorType: {
      propertyName: "minorType",
      propertyKey: "minorType",
      valueKey: "minor_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    note: {
      propertyName: "note",
      propertyKey: "note",
      valueKey: "note",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    prio: {
      propertyName: "prio",
      propertyKey: "prio",
      valueKey: "prio",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    prod: {
      propertyName: "prod",
      propertyKey: "prod",
      valueKey: "prod",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    prodAction: {
      propertyName: "prodAction",
      propertyKey: "prodAction",
      valueKey: "prod_action",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    refAction: {
      propertyName: "refAction",
      propertyKey: "refAction",
      valueKey: "ref_action",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    refTelegram: {
      propertyName: "refTelegram",
      propertyKey: "refTelegram.id",
      valueKey: "ref_telegram_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Telegram"
    },
    sent: {
      propertyName: "sent",
      propertyKey: "sent",
      valueKey: "sent",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    showNavigatorFlash: {
      propertyName: "showNavigatorFlash",
      propertyKey: "showNavigatorFlash",
      valueKey: "show_navigator_flash",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    slug: {
      propertyName: "slug",
      propertyKey: "slug",
      valueKey: "slug",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    source: {
      propertyName: "source",
      propertyKey: "source",
      valueKey: "source",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sourceLabel: {
      propertyName: "sourceLabel",
      propertyKey: "sourceLabel",
      valueKey: "source_label",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    status: {
      propertyName: "status",
      propertyKey: "status.id",
      valueKey: "status",
      propertyType: "MANY_TO_ONE",
      referenceType: "Status"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "TelegramSystemLink"
    },
    telegramUsers: {
      propertyName: "telegramUsers",
      propertyKey: "telegramUsers",
      valueKey: "telegram_user_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "TelegramUser"
    },
    textlength: {
      propertyName: "textlength",
      propertyKey: "textlength",
      valueKey: "textlength",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    userinfo: {
      propertyName: "userinfo",
      propertyKey: "userinfo",
      valueKey: "userinfo",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  TelegramSetting: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    metaTag: {
      propertyName: "metaTag",
      propertyKey: "metaTag",
      valueKey: "meta_tag",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    xmldata: {
      propertyName: "xmldata",
      propertyKey: "xmldata",
      valueKey: "xmldata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  TelegramSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    telegram: {
      propertyName: "telegram",
      propertyKey: "telegram.id",
      valueKey: "telegram_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Telegram"
    }
  },
  TelegramUser: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    product: {
      propertyName: "product",
      propertyKey: "product.id",
      valueKey: "product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Product"
    },
    subProduct: {
      propertyName: "subProduct",
      propertyKey: "subProduct.id",
      valueKey: "sub_product_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "SubProduct"
    },
    telegram: {
      propertyName: "telegram",
      propertyKey: "telegram.id",
      valueKey: "telegram_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Telegram"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  TrustedInformation: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    key: {
      propertyName: "key",
      propertyKey: "key",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    value: {
      propertyName: "value",
      propertyKey: "value",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  User: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    address: {
      propertyName: "address",
      propertyKey: "address",
      valueKey: "address",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    adminPrivileges: {
      propertyName: "adminPrivileges",
      propertyKey: "adminPrivileges",
      valueKey: "admin_privileges",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "AdminPrivilege"
    },
    allowedProducts: {
      propertyName: "allowedProducts",
      propertyKey: "allowedProducts",
      valueKey: "allowed_products",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Product"
    },
    articleLanguage: {
      propertyName: "articleLanguage",
      propertyKey: "articleLanguage",
      valueKey: "article_language",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    autoLogoutTime: {
      propertyName: "autoLogoutTime",
      propertyKey: "autoLogoutTime",
      valueKey: "auto_logout_time",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    cellular: {
      propertyName: "cellular",
      propertyKey: "cellular",
      valueKey: "cellular",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    city: {
      propertyName: "city",
      propertyKey: "city",
      valueKey: "city",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    cityLocation: {
      propertyName: "cityLocation",
      propertyKey: "cityLocation",
      valueKey: "city_location",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    company: {
      propertyName: "company",
      propertyKey: "company",
      valueKey: "company",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    country: {
      propertyName: "country",
      propertyKey: "country",
      valueKey: "country",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    countryLocation: {
      propertyName: "countryLocation",
      propertyKey: "countryLocation",
      valueKey: "country_location",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    createArticleMode: {
      propertyName: "createArticleMode",
      propertyKey: "createArticleMode",
      valueKey: "create_article_mode",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    custom1: {
      propertyName: "custom1",
      propertyKey: "custom1",
      valueKey: "custom1",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom2: {
      propertyName: "custom2",
      propertyKey: "custom2",
      valueKey: "custom2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom3: {
      propertyName: "custom3",
      propertyKey: "custom3",
      valueKey: "custom3",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom4: {
      propertyName: "custom4",
      propertyKey: "custom4",
      valueKey: "custom4",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    custom5: {
      propertyName: "custom5",
      propertyKey: "custom5",
      valueKey: "custom5",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    defaultProducts: {
      propertyName: "defaultProducts",
      propertyKey: "defaultProducts",
      valueKey: "default_products",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Product"
    },
    department: {
      propertyName: "department",
      propertyKey: "department.id",
      valueKey: "department_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Department"
    },
    departments: {
      propertyName: "departments",
      propertyKey: "departments",
      valueKey: "departments",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Department"
    },
    email: {
      propertyName: "email",
      propertyKey: "email",
      valueKey: "email",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    emailPrivate: {
      propertyName: "emailPrivate",
      propertyKey: "emailPrivate",
      valueKey: "email_private",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    expirationDate: {
      propertyName: "expirationDate",
      propertyKey: "expirationDate",
      valueKey: "expiration_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    external: {
      propertyName: "external",
      propertyKey: "external",
      valueKey: "external",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    fax: {
      propertyName: "fax",
      propertyKey: "fax",
      valueKey: "fax",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    firstName: {
      propertyName: "firstName",
      propertyKey: "firstName",
      valueKey: "fname",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    forcedTextColorize: {
      propertyName: "forcedTextColorize",
      propertyKey: "forcedTextColorize",
      valueKey: "forced_text_colorize",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    forcedTextLength: {
      propertyName: "forcedTextLength",
      propertyKey: "forcedTextLength",
      valueKey: "forced_text_length",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    initials: {
      propertyName: "initials",
      propertyKey: "initials",
      valueKey: "initials",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    lastActivity: {
      propertyName: "lastActivity",
      propertyKey: "lastActivity",
      valueKey: "last_activity",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    lastName: {
      propertyName: "lastName",
      propertyKey: "lastName",
      valueKey: "lname",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    latitude: {
      propertyName: "latitude",
      propertyKey: "latitude",
      valueKey: "latitude",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    login: {
      propertyName: "login",
      propertyKey: "login",
      valueKey: "login",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    longitude: {
      propertyName: "longitude",
      propertyKey: "longitude",
      valueKey: "longitude",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    mailPassword: {
      propertyName: "mailPassword",
      propertyKey: "mailPassword",
      valueKey: "mail_password",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mailUser: {
      propertyName: "mailUser",
      propertyKey: "mailUser",
      valueKey: "mail_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    organizations: {
      propertyName: "organizations",
      propertyKey: "organizations",
      valueKey: "organizations",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "Organization"
    },
    passwd: {
      propertyName: "passwd",
      propertyKey: "passwd",
      valueKey: "passwd",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    permanent: {
      propertyName: "permanent",
      propertyKey: "permanent",
      valueKey: "permanent",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    phone: {
      propertyName: "phone",
      propertyKey: "phone",
      valueKey: "phone",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    phone2: {
      propertyName: "phone2",
      propertyKey: "phone2",
      valueKey: "phone2",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    picture: {
      propertyName: "picture",
      propertyKey: "picture",
      valueKey: "picture",
      propertyType: "BASIC",
      propertyClass: "byte[]"
    },
    privilegeGroup: {
      propertyName: "privilegeGroup",
      propertyKey: "privilegeGroup.id",
      valueKey: "privilege_group_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "PrivilegeGroup"
    },
    removed: {
      propertyName: "removed",
      propertyKey: "removed",
      valueKey: "removed",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    sessions: {
      propertyName: "sessions",
      propertyKey: "sessions",
      valueKey: "user_session_infos",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "UserSessionInfo"
    },
    smtpServer: {
      propertyName: "smtpServer",
      propertyKey: "smtpServer",
      valueKey: "smtp_server",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    ssn: {
      propertyName: "ssn",
      propertyKey: "ssn",
      valueKey: "ssn",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    systemLinks: {
      propertyName: "systemLinks",
      propertyKey: "systemLinks",
      valueKey: "system_link_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "UserSystemLink"
    },
    titleinfo: {
      propertyName: "titleinfo",
      propertyKey: "titleinfo",
      valueKey: "titleinfo",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    userRole: {
      propertyName: "userRole",
      propertyKey: "userRole.id",
      valueKey: "user_role_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "UserRole"
    },
    userRoles: {
      propertyName: "userRoles",
      propertyKey: "userRoles",
      valueKey: "user_roles",
      propertyType: "MANY_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "UserRole"
    },
    userSettings: {
      propertyName: "userSettings",
      propertyKey: "userSettings",
      valueKey: "user_settings",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    zipCode: {
      propertyName: "zipCode",
      propertyKey: "zipCode",
      valueKey: "zip_code",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  UserBylineImage: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageId: {
      propertyName: "imageId",
      propertyKey: "imageId",
      valueKey: "image_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    imageName: {
      propertyName: "imageName",
      propertyKey: "imageName",
      valueKey: "image_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  UserDataConfig: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    xmldata: {
      propertyName: "xmldata",
      propertyKey: "xmldata",
      valueKey: "xmldata",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  UserRole: {
    category: {
      propertyName: "category",
      propertyKey: "category",
      valueKey: "category",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.UserRole.Category"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    permanent: {
      propertyName: "permanent",
      propertyKey: "permanent",
      valueKey: "is_permanent",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    shortName: {
      propertyName: "shortName",
      propertyKey: "shortName",
      valueKey: "short_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  UserSessionInfo: {
    clientInfo: {
      propertyName: "clientInfo",
      propertyKey: "clientInfo",
      valueKey: "client_info",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    firstStamp: {
      propertyName: "firstStamp",
      propertyKey: "firstStamp",
      valueKey: "first_stamp",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    lastStamp: {
      propertyName: "lastStamp",
      propertyKey: "lastStamp",
      valueKey: "last_stamp",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  UserSystemLink: {
    externalId: {
      propertyName: "externalId",
      propertyKey: "externalId",
      valueKey: "external_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalSystem: {
      propertyName: "externalSystem",
      propertyKey: "externalSystem.id",
      valueKey: "external_system_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExternalSystem"
    },
    externalUri: {
      propertyName: "externalUri",
      propertyKey: "externalUri",
      valueKey: "external_uri",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    externalUser: {
      propertyName: "externalUser",
      propertyKey: "externalUser",
      valueKey: "external_user",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    user: {
      propertyName: "user",
      propertyKey: "user.id",
      valueKey: "user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    }
  },
  ValueMap: {
    entityType: {
      propertyName: "entityType",
      propertyKey: "entityType",
      valueKey: "entity_type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    groupId: {
      propertyName: "groupId",
      propertyKey: "groupId",
      valueKey: "group_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    mapKey: {
      propertyName: "mapKey",
      propertyKey: "mapKey",
      valueKey: "key",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    mapValue: {
      propertyName: "mapValue",
      propertyKey: "mapValue",
      valueKey: "value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    subGroupId: {
      propertyName: "subGroupId",
      propertyKey: "subGroupId",
      valueKey: "sub_group_id",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  WorkflowTrigger: {
    active: {
      propertyName: "active",
      propertyKey: "active",
      valueKey: "active",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    allowUserExecution: {
      propertyName: "allowUserExecution",
      propertyKey: "allowUserExecution",
      valueKey: "allow_user_execution",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    description: {
      propertyName: "description",
      propertyKey: "description",
      valueKey: "description",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    execCount: {
      propertyName: "execCount",
      propertyKey: "execCount",
      valueKey: "exec_count",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    execStarted: {
      propertyName: "execStarted",
      propertyKey: "execStarted",
      valueKey: "exec_started",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    execStopped: {
      propertyName: "execStopped",
      propertyKey: "execStopped",
      valueKey: "exec_stopped",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    organization: {
      propertyName: "organization",
      propertyKey: "organization.id",
      valueKey: "organization_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "Organization"
    },
    parameters: {
      propertyName: "parameters",
      propertyKey: "parameters",
      valueKey: "parameters",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    scheduledDate: {
      propertyName: "scheduledDate",
      propertyKey: "scheduledDate",
      valueKey: "scheduled_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    serviceName: {
      propertyName: "serviceName",
      propertyKey: "serviceName",
      valueKey: "service_name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    status: {
      propertyName: "status",
      propertyKey: "status",
      valueKey: "status",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.WorkflowTrigger.Status"
    },
    statusInfo: {
      propertyName: "statusInfo",
      propertyKey: "statusInfo",
      valueKey: "status_info",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    triggerTask: {
      propertyName: "triggerTask",
      propertyKey: "triggerTask.id",
      valueKey: "trigger_task_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "WorkflowTrigger"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "se.infomaker.newspilot.entity.TriggerType"
    },
    updatedDate: {
      propertyName: "updatedDate",
      propertyKey: "updatedDate",
      valueKey: "updated_date",
      propertyType: "BASIC",
      propertyClass: "java.util.Date"
    },
    updatedUser: {
      propertyName: "updatedUser",
      propertyKey: "updatedUser.id",
      valueKey: "updated_user_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "User"
    },
    xml: {
      propertyName: "xml",
      propertyKey: "xml",
      valueKey: "xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  XmlElement: {
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    internal: {
      propertyName: "internal",
      propertyKey: "internal",
      valueKey: "internal",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    name: {
      propertyName: "name",
      propertyKey: "name",
      valueKey: "name",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    sortOrder: {
      propertyName: "sortOrder",
      propertyKey: "sortOrder",
      valueKey: "sort_order",
      propertyType: "BASIC",
      propertyClass: "java.lang.Double"
    },
    type: {
      propertyName: "type",
      propertyKey: "type",
      valueKey: "type",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    }
  },
  XmlElementConfig: {
    articlePartConfigs: {
      propertyName: "articlePartConfigs",
      propertyKey: "articlePartConfigs",
      valueKey: "article_part_config_ids",
      propertyType: "ONE_TO_MANY",
      propertyClass: "java.util.Set",
      referenceType: "ArticlePartConfig"
    },
    crc: {
      propertyName: "crc",
      propertyKey: "crc",
      valueKey: "crc",
      propertyType: "BASIC",
      propertyClass: "java.lang.Long"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    xml: {
      propertyName: "xml",
      propertyKey: "xml",
      valueKey: "xml",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    }
  },
  XmlElementExport: {
    endValue: {
      propertyName: "endValue",
      propertyKey: "endValue",
      valueKey: "end_value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    exportConfig: {
      propertyName: "exportConfig",
      propertyKey: "exportConfig.id",
      valueKey: "export_config_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "ExportConfig"
    },
    extra: {
      propertyName: "extra",
      propertyKey: "extra",
      valueKey: "extra",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    id: {
      propertyName: "id",
      propertyKey: "id",
      valueKey: "id",
      propertyType: "BASIC",
      propertyClass: "java.lang.Integer"
    },
    noExport: {
      propertyName: "noExport",
      propertyKey: "noExport",
      valueKey: "no_export",
      propertyType: "BASIC",
      propertyClass: "java.lang.Boolean"
    },
    startValue: {
      propertyName: "startValue",
      propertyKey: "startValue",
      valueKey: "start_value",
      propertyType: "BASIC",
      propertyClass: "java.lang.String"
    },
    xmlElement: {
      propertyName: "xmlElement",
      propertyKey: "xmlElement.id",
      valueKey: "xml_element_id",
      propertyType: "MANY_TO_ONE",
      referenceType: "XmlElement"
    }
  }
};
