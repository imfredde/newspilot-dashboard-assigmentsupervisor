/*
|--------------------------------------------------------------------------
| Main.js - The starting point for your plugin
|--------------------------------------------------------------------------
|
| main.js is responsible for importing all plugin core resources.
| Below we are importing the sass file the plugin styles and the plugin file itself.
|
*/
//<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
require("./components/plugin/style/servicecards.scss");
require("./components/plugin/style/style.scss");
require("./components/plugin/style/semantic/semantic.min.css");
require("./components/plugin/index.jsx");
